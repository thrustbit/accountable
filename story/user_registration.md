**User Registration**


* User can register with 
  * Id given by domain
  * Unique username
  * Unique email address
  * Valid password
  
* Flow
  * RegisterUser command
  * UserWasRegistered Event
  