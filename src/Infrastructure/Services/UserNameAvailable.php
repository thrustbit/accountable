<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Infrastructure\Services;

use Thrustbit\Accountable\Domain\Account\Services\UserNameAvailability;
use Thrustbit\Accountable\Projection\User\UserFinder;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;
use Thrustbit\DevDomain\Application\Values\UserName\UserName;

class UserNameAvailable implements UserNameAvailability
{
    /**
     * @var UserFinder
     */
    private $userFinder;

    public function __construct(UserFinder $userFinder)
    {
        $this->userFinder = $userFinder;
    }

    public function __invoke(UserName $userName): ?UserId
    {
        $user = $this->userFinder->userOfUserName($userName->identify());

        return $user ? $user->getId() : null;
    }
}