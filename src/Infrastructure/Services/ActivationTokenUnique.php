<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Infrastructure\Services;

use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Repository\ActivationRead;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Values\ActivationToken;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Services\ActivationTokenAvailability;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class ActivationTokenUnique implements ActivationTokenAvailability
{
    /**
     * @var ActivationRead
     */
    private $activationRead;

    public function __construct(ActivationRead $activationRead)
    {
        $this->activationRead = $activationRead;
    }

    public function __invoke(ActivationToken $activationToken): ?UserId
    {
        $activation = $this->activationRead->activationOfToken($activationToken->identify());

        return $activation ? $activation->getId() : null;
    }
}