<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Infrastructure\Services;

use Thrustbit\Accountable\Domain\Role\Services\RoleNameAvailability;
use Thrustbit\Accountable\Domain\Role\Values\RoleId;
use Thrustbit\Accountable\Domain\Role\Values\RoleName;
use Thrustbit\Accountable\Projection\Role\RoleFinder;

class RoleNameAvailable implements RoleNameAvailability
{
    /**
     * @var RoleFinder
     */
    private $roleFinder;

    public function __construct(RoleFinder $roleFinder)
    {
        $this->roleFinder = $roleFinder;
    }

    public function __invoke(RoleName $roleName): ?RoleId
    {
        $role = $this->roleFinder->roleOfName($roleName->getName());

        return $role ? $role->getId() : null;
    }
}