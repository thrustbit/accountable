<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Infrastructure\Services;

use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Services\FirstActiveResetTokenMatcher;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\PasswordReset;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Repository\ResetPasswordRead;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Values\PasswordResetToken;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;

class MatchFirstActiveResetToken implements FirstActiveResetTokenMatcher
{
    /**
     * @var ResetPasswordRead
     */
    private $resetPasswordRead;

    public function __construct(ResetPasswordRead $resetPasswordRead)
    {
        $this->resetPasswordRead = $resetPasswordRead;
    }

    public function __invoke(EmailAddress $email, PasswordResetToken $token): ?PasswordReset
    {
        return $this->resetPasswordRead
            ->passwordResetOfUserEmail($email)
            ->matchFirstPending($token);
    }
}