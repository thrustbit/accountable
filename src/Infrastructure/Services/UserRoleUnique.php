<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Infrastructure\Services;

use Thrustbit\Accountable\Domain\Account\Model\Enrole\Exceptions\RoleAlreadyAttached;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Repository\EnroleRead;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Services\UserRoleAvailable;
use Thrustbit\Accountable\Domain\Role\Exceptions\RoleNotFound;
use Thrustbit\Accountable\Domain\Role\Repository\RoleRead;
use Thrustbit\Accountable\Domain\Role\Role;
use Thrustbit\DevDomain\Application\Values\Identifier;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class UserRoleUnique implements UserRoleAvailable
{
    /**
     * @var EnroleRead
     */
    private $enroleRead;

    /**
     * @var RoleRead
     */
    private $roleRead;

    public function __construct(EnroleRead $enroleRead, RoleRead $roleRead)
    {
        $this->enroleRead = $enroleRead;
        $this->roleRead = $roleRead;
    }

    /**
     * @param UserId $userId
     * @param Identifier $identifier
     * @return null|Role
     *
     * @throws RoleNotFound
     * @throws RoleAlreadyAttached
     */
    public function __invoke(UserId $userId, Identifier $identifier): ?Role
    {
        if ($this->hasNotRole($userId, $identifier)) {
            $role = $this->roleRead->roleOfIdentifier($identifier);

            if (!$role) {
                throw RoleNotFound::withIdentifier($identifier);
            }

            return $role;
        }

        throw RoleAlreadyAttached::withUserId($userId, $identifier);
    }

    private function hasNotRole(UserId $userId, Identifier $identifier): bool
    {
        // checkMe optimize query
        return $this->enroleRead
            ->rolesOfUserId($userId->identify())
            ->hasNotRole($identifier);
    }
}