<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Infrastructure\Services;

use Thrustbit\Accountable\Domain\Account\Services\EmailAvailability;
use Thrustbit\Accountable\Projection\User\UserFinder;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class EmailAvailable implements EmailAvailability
{
    /**
     * @var UserFinder
     */
    private $userFinder;

    public function __construct(UserFinder $userFinder)
    {
        $this->userFinder = $userFinder;
    }

    public function __invoke(EmailAddress $email): ?UserId
    {
        $user = $this->userFinder->userOfEmail($email->identify());

        return $user ? $user->getId() : null;
    }
}