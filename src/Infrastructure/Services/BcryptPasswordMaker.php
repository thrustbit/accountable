<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Infrastructure\Services;

use Illuminate\Contracts\Hashing\Hasher;
use Thrustbit\Accountable\Domain\Account\Services\PasswordMaker;
use Thrustbit\DevDomain\Application\Values\Contracts\EncodedPassword;
use Thrustbit\DevDomain\Application\Values\Credentials\BcryptPassword;
use Thrustbit\DevDomain\Application\Values\Credentials\ClearPassword;

class BcryptPasswordMaker implements PasswordMaker
{
    /**
     * @var Hasher
     */
    private $encoder;

    public function __construct(Hasher $encoder)
    {
        $this->encoder = $encoder;
    }

    public function check(ClearPassword $clearPassword, EncodedPassword $encodedPassword): bool
    {
        return $this->encoder->check($clearPassword->getCredentials(), $encodedPassword->getCredentials());
    }

    public function make(ClearPassword $clearPassword): EncodedPassword
    {
        $encodedPassword = $this->encoder->make($clearPassword->getCredentials());

        return BcryptPassword::fromString($encodedPassword);
    }

    public function reHash(ClearPassword $clearPassword, EncodedPassword $encodedPassword): ?EncodedPassword
    {
        $reHashedPassword = null;

        if (!$this->check($clearPassword, $encodedPassword)) {
            return $reHashedPassword;
        }

        if ($this->encoder->needsRehash($encodedPassword->getCredentials())) {
            $reHashedPassword = $this->make($clearPassword);
        }

        return BcryptPassword::fromString($reHashedPassword);
    }

    public function __invoke(ClearPassword $clearPassword): EncodedPassword
    {
        return $this->make($clearPassword);
    }
}