<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Infrastructure\Repository;

use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Repository\ThrottleCollection;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Repository\ThrottleRead;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Throttle;
use Thrustbit\ModelEvent\Model\Repository\EventModelRepository;
use Thrustbit\ModelEvent\ModelRoot;

class ThrottleRepository extends EventModelRepository implements ThrottleCollection
{
    public function get(string $userId): ?Throttle
    {
        return $this->readModel->throttleOfUserId($userId);
    }

    public function save(Throttle $model): void
    {
        $this->saveAggregateRoot($model);
    }

    public function assertModelType(ModelRoot $model): void
    {
        if (!$model instanceof Throttle && !$this->readModel instanceof ThrottleRead) {
            throw new \RuntimeException('Wrong model type');
        }
    }
}