<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Infrastructure\Repository;

use Thrustbit\Accountable\Domain\Account\Repository\UserCollection;
use Thrustbit\Accountable\Domain\Account\Repository\UserRead;
use Thrustbit\Accountable\Domain\Account\User;
use Thrustbit\ModelEvent\Model\Repository\EventModelRepository;
use Thrustbit\ModelEvent\ModelRoot;

class UserRepository extends EventModelRepository implements UserCollection
{
    public function get(string $userId): ?User
    {
        return $this->readModel->userOfid($userId);
    }

    public function save(User $modelRoot): void
    {
        $this->saveAggregateRoot($modelRoot);
    }

    public function assertModelType(ModelRoot $model): void
    {
        if (!$model instanceof User && !$this->readModel instanceof UserRead) {
            throw new \RuntimeException('Wrong model type');
        }
    }
}