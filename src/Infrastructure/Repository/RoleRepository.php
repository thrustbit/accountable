<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Infrastructure\Repository;

use Thrustbit\Accountable\Domain\Role\Repository\RoleList;
use Thrustbit\Accountable\Domain\Role\Repository\RoleRead;
use Thrustbit\Accountable\Domain\Role\Role;
use Thrustbit\ModelEvent\Model\Repository\EventModelRepository;
use Thrustbit\ModelEvent\ModelRoot;

class RoleRepository extends EventModelRepository implements RoleList
{
    public function get(string $roleId): ?Role
    {
        return $this->readModel->roleOfId($roleId);
    }

    public function save(Role $role): void
    {
        $this->saveAggregateRoot($role);
    }

    public function assertModelType(ModelRoot $model): void
    {
        if (!$model instanceof Role && !$this->readModel instanceof RoleRead) {
            throw new \RuntimeException('Wrong model type');
        }
    }
}