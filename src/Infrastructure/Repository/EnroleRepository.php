<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Infrastructure\Repository;

use Thrustbit\Accountable\Domain\Account\Model\Enrole\Enrole;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Repository\EnroleCollection;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Repository\EnroleRead;
use Thrustbit\ModelEvent\Model\Repository\EventModelRepository;
use Thrustbit\ModelEvent\ModelRoot;

class EnroleRepository extends EventModelRepository implements EnroleCollection
{
    public function get(string $aggregateId): ?Enrole
    {
        return $this->readModel->enroleOfId($aggregateId);
    }

    public function save(Enrole $root): void
    {
        $this->saveAggregateRoot($root);
    }

    public function assertModelType(ModelRoot $model): void
    {
        if (!$model instanceof Enrole && !$this->readModel instanceof EnroleRead) {
            throw new \RuntimeException('Wrong model type');
        }
    }
}