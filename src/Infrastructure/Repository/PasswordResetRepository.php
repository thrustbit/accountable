<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Infrastructure\Repository;

use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\PasswordReset;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Repository\ResetPasswordCollection;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Repository\ResetPasswordRead;
use Thrustbit\ModelEvent\Model\Repository\EventModelRepository;
use Thrustbit\ModelEvent\ModelRoot;

class PasswordResetRepository extends EventModelRepository implements ResetPasswordCollection
{
    public function get(string $aggregateId): ?PasswordReset
    {
        return $this->readModel->passwordResetOfId($aggregateId);
    }

    public function save(PasswordReset $model): void
    {
        $this->saveAggregateRoot($model);
    }

    public function assertModelType(ModelRoot $model): void
    {
        if (!$model instanceof PasswordReset && !$this->readModel instanceof ResetPasswordRead) {
            throw new \RuntimeException('Wrong model type');
        }
    }
}