<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Infrastructure\Repository;

use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Activation;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Repository\ActivationCollection;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Repository\ActivationRead;
use Thrustbit\ModelEvent\Model\Repository\EventModelRepository;
use Thrustbit\ModelEvent\ModelRoot;

class ActivationRepository extends EventModelRepository implements ActivationCollection
{
    public function get(string $aggregateId): ?Activation
    {
        return $this->readModel->activationOfUserId($aggregateId);
    }

    public function save(Activation $root): void
    {
        $this->saveAggregateRoot($root);
    }

    public function assertModelType(ModelRoot $model): void
    {
        if (!$model instanceof Activation && !$this->readModel instanceof ActivationRead) {
            throw new \RuntimeException('Wrong model type');
        }
    }
}