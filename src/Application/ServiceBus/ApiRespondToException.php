<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\ServiceBus;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Accountable\Application\Exceptions\InvalidArgumentException;
use Thrustbit\Accountable\Domain\Account\Exceptions\BadCredentials;
use Thrustbit\Accountable\Domain\Account\Exceptions\UserStatusException;
use Thrustbit\DevApi\Http\Response\ApiResponse;
use Thrustbit\DevDomain\Application\Exceptions\ValidationFailed;

class ApiRespondToException implements ApiResponse
{
    /* https://github.com/ellipsesynergie/api-response/blob/master/src/AbstractResponse.php */
    const CODE_WRONG_ARGS = 'GEN-WRONG-ARGS';
    const CODE_NOT_FOUND = 'GEN-NOT-FOUND';
    const CODE_INTERNAL_ERROR = 'GEN-INTERNAL-ERROR';
    const CODE_UNAUTHORIZED = 'GEN-UNAUTHORIZED';
    const CODE_FORBIDDEN = 'GEN-FORBIDDEN';
    const CODE_GONE = 'GEN-GONE';
    const CODE_METHOD_NOT_ALLOWED = 'GEN-METHOD-NOT-ALLOWED';
    const CODE_UNWILLING_TO_PROCESS = 'GEN-UNWILLING-TO-PROCESS';
    const CODE_UNPROCESSABLE = 'GEN-UNPROCESSABLE';

    /**
     * @var Application
     */
    private $app;

    /**
     * @var int
     */
    private $statusCode;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function respondTo(\Throwable $exception): Response
    {
        return $this->handleException($exception);
    }

    private function handleException(\Throwable $exception)
    {
        if (!$exception instanceof MessageDispatchException) {
            throw $exception;
        }

        if (!$exception = $exception->getPrevious()) {
            throw $exception;
        }

        if ($exception instanceof InvalidArgumentException) {
            return $this->errorInternalError();
        }

        if ($exception instanceof ValidationFailed) {
            return $this->errorUnprocessable($exception->getMessage());
        }

        if ($exception instanceof BadCredentials) {
            return $this->errorUnauthorized($exception->getMessage());
        }

        if ($exception instanceof UserStatusException) {
            return $this->errorForbidden($exception->getMessage());
        }

        return $this->errorWrongArgs($exception->getMessage());
    }

    public function withError(string $message, string $errorCode, array $headers = []): JsonResponse
    {
        return $this->withResponse([
            'error' => [
                'code' => $errorCode,
                'http_code' => $this->statusCode,
                'message' => $message
            ]
        ],
            $headers
        );
    }

    public function errorForbidden(string $message = null, array $headers = []): JsonResponse
    {
        $message = $message ?? 'Forbidden';

        return $this->setStatusCode(403)->withError($message, static::CODE_FORBIDDEN, $headers);
    }

    public function errorInternalError(string $message = null, array $headers = []): JsonResponse
    {
        $message = $message ?? 'Internal Error';

        return $this->setStatusCode(500)->withError($message, static::CODE_INTERNAL_ERROR, $headers);
    }

    public function errorNotFound(string $message = null, array $headers = []): JsonResponse
    {
        $message = $message ?? 'Resource Not Found';

        return $this->setStatusCode(404)->withError($message, static::CODE_NOT_FOUND, $headers);
    }

    public function errorUnauthorized(string $message = null, array $headers = []): JsonResponse
    {
        $message = $message ?? 'Unauthorized';

        return $this->setStatusCode(401)->withError($message, static::CODE_UNAUTHORIZED, $headers);
    }

    public function errorWrongArgs(string $message = null, array $headers = []): JsonResponse
    {
        $message = $message ?? 'Wrong Arguments';

        return $this->setStatusCode(400)->withError($message, static::CODE_WRONG_ARGS, $headers);
    }

    public function errorGone(string $message = null, array $headers = []): JsonResponse
    {
        $message = $message ?? 'Resource no longer available';

        return $this->setStatusCode(410)->withError($message, static::CODE_GONE, $headers);
    }

    public function errorMethodNotAllowed(string $message = null, array $headers = []): JsonResponse
    {
        $message = $message ?? 'Method Not Allowed';

        return $this->setStatusCode(405)->withError($message, static::CODE_METHOD_NOT_ALLOWED, $headers);
    }

    public function errorUnwillingToProcess(string $message = null, array $headers = []): JsonResponse
    {
        $message = $message ?? 'Server is unwilling to process the request';

        return $this->setStatusCode(431)->withError($message, static::CODE_UNWILLING_TO_PROCESS, $headers);
    }

    public function errorUnprocessable(string $message = null, array $headers = []): JsonResponse
    {
        $message = $message ?? 'Unprocessable Entity';

        return $this->setStatusCode(422)->withError($message, static::CODE_UNPROCESSABLE, $headers);
    }

    protected function withResponse(array $data, array $headers): JsonResponse
    {
        return new JsonResponse($data, $this->statusCode, $headers);
    }

    protected function setStatusCode(int $statusCode): self
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function isDevEnvironment(): bool
    {
        return $this->app->environment() !== 'production';
    }
}