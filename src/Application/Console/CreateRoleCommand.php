<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Console;

use Illuminate\Console\Command;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Thrustbit\Accountable\Domain\Role\Command\CreateRole;
use Thrustbit\Accountable\Domain\Role\Values\RoleId;

class CreateRoleCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'accountable:create-role {role*}';

    /**
     * @var string
     */
    protected $description = 'Create role with name and description';
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
    }

    public function handle(): void
    {
        try {
            $this->commandBus->dispatch(new CreateRole([
                'role_id' => RoleId::nextIdentity()->identify(),
                'role_name' => $this->arguments()['role'][0],
                'role_description' => $this->arguments()['role'][1]
            ]));

            $this->info('Role created');
        } catch (MessageDispatchException $exception) {
            $this->error($exception->getPrevious()->getMessage());
        }
    }
}