<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Console;

use Faker\Factory;
use Faker\Generator;
use Illuminate\Console\Command;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Prooph\ServiceBus\QueryBus;
use Thrustbit\Accountable\Domain\Account\Command\ChangeEmail;
use Thrustbit\Accountable\Domain\Account\Command\ChangePassword;
use Thrustbit\Accountable\Domain\Account\Command\ChangeUserName;
use Thrustbit\Accountable\Domain\Account\Command\RegisterUser;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Command\RegisterResetPassword;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Command\UserResetPassword;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\PasswordReset;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Query\GetPasswordResetByUserEmail;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Command\ActivateUserWithActivationToken;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Activation;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Query\GetUserActivationByUserId;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class TestAppCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'accountable:test-app';

    /**
     * @var string
     */
    protected $description = 'Test app';
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var Generator
     */
    private $faker;

    public function __construct(CommandBus $commandBus, QueryBus $queryBus, Factory $faker)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
        $this->faker = $faker::create();
    }

    public function handle(): void
    {
        try {
            $userData = $this->registerUser();
            $userId = $userData['user_id'];

            $this->activateUser($userId);

            $newPassword = $this->resetPassword($userData['email'], $userId);

            $this->changePassword($userId, $newPassword, $userData['password']);

            $this->changeEmail($userId);

            $this->changeUserName($userId);

        } catch (MessageDispatchException $exception) {
            $this->handleException($exception);
        }
    }

    private function registerUser(): array
    {
        $this->commandBus->dispatch(
            new RegisterUser($data = [
                'user_id' => UserId::nextIdentity()->identify(),
                'user_name' => $this->faker->userName,
                'email' => $this->faker->email,
                'password' => 'password',
                'password_confirmation' => 'password'
            ])
        );

        $this->info('User registered with email' . $data['email']);

        return $data;
    }

    private function activateUser(string $userId)
    {
        // handler find token itself
        $activation = $this->getUserActivation($userId);
        $this->handleException($activation);


        $this->commandBus->dispatch(new ActivateUserWithActivationToken([
            //'user_id' => $userId,
            'activation_token' => $activation->getActivationToken()->identify()
        ]));

        $this->info('User activated');
    }

    private function changePassword(string $userId, string $oldPassword, string $newPassword)
    {
        $this->commandBus->dispatch(new ChangePassword([
            'user_id' => $userId,
            'current_password' => $oldPassword,
            'new_password' => $newPassword,
            'new_password_confirmation' => $newPassword,
        ]));

        $this->info('Password has been changed to original');
    }

    private function changeEmail(string $userId): string
    {
        $this->commandBus->dispatch(new ChangeEmail([
            'user_id' => $userId,
            'new_email' => $newEmail = $this->faker->email
        ]));

        $this->info(sprintf('Email was changed to %s', $newEmail));

        return $newEmail;
    }

    private function changeUserName(string $userId): string
    {
        $this->commandBus->dispatch(new ChangeUserName([
            'user_id' => $userId,
            'new_user_name' => $newUserName = $this->faker->userName
        ]));

        $this->info(sprintf('User name was changed to %s', $newUserName));

        return $newUserName;
    }

    private function resetPassword(string $email, string $userId): string
    {
        // application layer must define the reset token
        $this->commandBus->dispatch(new RegisterResetPassword([
            'email' => $email
        ]));

        $reset = $this->getResetPassword($email);
        $this->handleException($reset);

        $this->commandBus->dispatch(new UserResetPassword([
            'user_id' => $userId,
            'password_reset_token' => $reset->getPasswordResetToken()->identify(),
            'new_password' => $newPassword = 'password1',
            'new_password_confirmation' => $newPassword,
        ]));

        $this->info('User reset password');

        return $newPassword;
    }

    /**
     * @param string $email
     * @return null|MessageDispatchException|PasswordReset
     */
    private function getResetPassword(string $email)
    {
        $user = null;
        $exception = null;
        $this->queryBus->dispatch(new GetPasswordResetByUserEmail(['email' => $email]))
            ->then(function ($data) use (&$user) {
                $user = $data;
            }, function ($e) use (&$exception) {
                $exception = $e;
            });

        return $exception ?? $user;
    }

    /**
     * @param string $userId
     * @return null|MessageDispatchException|Activation
     */
    private function getUserActivation(string $userId)
    {
        $user = null;
        $exception = null;
        $this->queryBus->dispatch(new GetUserActivationByUserId(['user_id' => $userId]))
            ->then(function ($data) use (&$user) {
                $user = $data;
            }, function ($e) use (&$exception) {
                $exception = $e;
            });

        return $exception ?? $user;
    }

    private function handleException($e): void
    {
        if (!$e) {
            $this->error('Null result for one query');
            die(1);
        }

        if ($e instanceof MessageDispatchException) {
            $this->error($e->getPrevious()->getMessage());
            die(1);
        }
    }
}