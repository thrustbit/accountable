<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Http\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use React\Promise\PromiseInterface;
use Thrustbit\Accountable\Application\Exceptions\InvalidArgumentException;
use Thrustbit\DevDomain\Application\Exceptions\DomainException;
use Thrustbit\ServiceBus\ServiceBus;

class ApiResource
{
    /**
     * @var ServiceBus
     */
    protected $bus;

    public function __construct(ServiceBus $bus)
    {
        $this->bus = $bus;
    }

    protected function buildResponse(string $message, int $statusCode, array $extras = []): JsonResponse
    {
        return new JsonResponse([
            'data' => [
                'message' => $message,
                'code' => $statusCode,
                'extra' => $extras
            ]
        ]);
    }

    protected function successResponse(string $message, int $statusCode = 302, array $extras = []): JsonResponse
    {
        return $this->buildResponse($message, $statusCode, $extras);
    }

    protected function failureResponse(string $message, int $statusCode = 423, array $extras = []): JsonResponse
    {
        return $this->buildResponse($message, $statusCode, $extras);
    }

    protected function handleExceptionMessage(MessageDispatchException $exception): string
    {
        $previousException = $exception->getPrevious();

        if (!$previousException || $previousException instanceof InvalidArgumentException) {
            return 'An error occurred ... Try again!';
        }

        // handle Validation failed

        if (!$previousException instanceof DomainException) {
            throw $previousException;
        }

        return $exception->getPrevious()->getMessage();
    }

    /**
     * @param PromiseInterface $promise
     *
     * @return MessageDispatchException|Collection
     */
    protected function fromPromise(PromiseInterface $promise)
    {
        $result = null;
        $exception = null;

        $promise->then(function ($data) use (&$result) {
            $result = $data;
        }, function ($e) use (&$exception) {
            $exception = $e;
        });

        return $exception ?? $result;
    }
}