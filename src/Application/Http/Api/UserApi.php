<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Http\Api;

use Illuminate\Http\Request;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Thrustbit\Accountable\Application\Http\Resources\UserResource;
use Thrustbit\Accountable\Domain\Account\LocalUser;
use Thrustbit\Accountable\Domain\Account\Query\GetUserById;

class UserApi extends ApiResource
{
    public function __invoke(Request $request, string $userId)
    {
        $result = $this->dispatchQuery($request, $userId);

        if ($result instanceof MessageDispatchException) {
            return $this->failureResponse(
                $this->handleExceptionMessage($result)
            );
        }

        if (null === $result || !$result instanceof LocalUser) {
            return $this->failureResponse(
                sprintf('User not found with id %s', $userId)
            );
        }

        return new UserResource($result);

        return $this->successResponse('done', 302, ['users' => $result]);
    }

    private function dispatchQuery(Request $request, string $userId)
    {
        return $this->fromPromise(
            $this->bus->dispatch(new GetUserById([
                'user_id' => $userId
            ]))
        );
    }
}