<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Http\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Thrustbit\Accountable\Domain\Account\Query\GetUsers;

class UserResource extends ApiResource
{

    public function __invoke(Request $request):JsonResponse
    {
        $result = $this->dispatchQuery($request);

        if ($result instanceof MessageDispatchException) {
            return $this->failureResponse(
                $this->handleExceptionMessage($result)
            );
        }

        return $this->successResponse('done', 302,['users' => $result]);
    }

    private function dispatchQuery(Request $request)
    {
        return $this->fromPromise(
            $this->bus->dispatch(new GetUsers([]))
        );
    }
}