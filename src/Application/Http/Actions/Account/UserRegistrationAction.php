<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Http\Actions\Account;

use Faker\Factory;
use Illuminate\Http\Request;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Accountable\Application\Http\Actions\HttpAction;
use Thrustbit\Accountable\Domain\Account\Command\RegisterUser;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;


class UserRegistrationAction extends HttpAction
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct(CommandBus $commandBus, Factory $factory)
    {
        $this->commandBus = $commandBus;
        $this->faker = $factory::create();
    }

    public function __invoke(Request $request): Response
    {
        try {
            $this->commandBus->dispatch($message = $this->command($request));
        } catch (MessageDispatchException $exception) {
            return $this->failureResponse($this->handleExceptionMessage($exception));
        }

        return $this->successResponse('User has been registered', 302, $message->toArray());
    }

    protected function command(Request $request): RegisterUser
    {
        return new RegisterUser([
            'user_id' => UserId::nextIdentity()->identify(),
            'user_name' => $this->faker->userName,
            'email' => $this->faker->email,
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);
    }
}