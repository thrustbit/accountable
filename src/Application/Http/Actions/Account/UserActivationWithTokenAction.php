<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Http\Actions\Account;

use Illuminate\Http\Request;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Prooph\ServiceBus\QueryBus;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Accountable\Application\Http\Actions\HttpAction;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Command\ActivateUserWithActivationToken;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Query\GetUserByActivationToken;

class UserActivationWithTokenAction extends HttpAction
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var QueryBus
     */
    private $queryBus;

    public function __construct(CommandBus $commandBus, QueryBus $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }

    public function __invoke(Request $request, string $activationToken): Response
    {
        $activation = $this->getUserActivationByToken($activationToken);

        if (!$activation) {
            return $this->failureResponse(
                'Activation token not found, invalid or has been expired'
            );
        }

        if ($activation instanceof MessageDispatchException) {
            return $this->failureResponse(
                $this->handleExceptionMessage($activation)
            );
        }

        try {
            $this->commandBus->dispatch(new ActivateUserWithActivationToken([
                'user_id' => $activation->getId()->identify(),
                'activation_token' => $activationToken
            ]));
        } catch (CommandDispatchException $exception) {
            return $this->failureResponse(
                $this->handleExceptionMessage($exception)
            );
        }

        return $this->successResponse('User has been activated', 302);
    }

    private function getUserActivationByToken(string $activationToken)
    {
        return $this->fromPromise(
            $this->queryBus->dispatch(
                new GetUserByActivationToken(['activation_token' => $activationToken])
            )
        );
    }
}