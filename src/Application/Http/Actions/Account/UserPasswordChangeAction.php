<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Http\Actions\Account;

use Illuminate\Http\Request;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Accountable\Application\Http\Actions\HttpAction;
use Thrustbit\Accountable\Domain\Account\Command\ChangePassword;

class UserPasswordChangeAction extends HttpAction
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(Request $request): Response
    {
        try {
            $this->commandBus->dispatch(
                new ChangePassword([
                    'user_id' => $request->input('user_id'),
                    'current_password' => $request->input('current_password'),
                    'new_password' => $request->input('new_password'),
                    'new_password_confirmation' => $request->input('new_password_confirmation'),
                ])
            );
        } catch (MessageDispatchException $exception) {
            return $this->failureResponse(
                $this->handleExceptionMessage($exception)
            );
        }

        return $this->successResponse('Password has been updated');
    }
}