<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Http\Actions\Account;

use Illuminate\Http\Request;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Thrustbit\Accountable\Application\Http\Actions\HttpAction;
use Thrustbit\Accountable\Domain\Account\Command\ChangeEmail;

class UserEmailChangeAction extends HttpAction
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(Request $request)
    {
        try {
            $this->commandBus->dispatch(
                new ChangeEmail([
                    'user_id' => $request->input('user_id'),
                    'new_email' => $request->input('new_email')
                ])
            );
        } catch (MessageDispatchException $exception) {
            return $this->failureResponse(
                $this->handleExceptionMessage($exception)
            );
        }

        return $this->successResponse('Email has been updated.');
    }
}