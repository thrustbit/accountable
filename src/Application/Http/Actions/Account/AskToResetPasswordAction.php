<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Http\Actions\Account;

use Illuminate\Http\Request;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Thrustbit\Accountable\Application\Http\Actions\HttpAction;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Command\RegisterResetPassword;

class AskToResetPasswordAction extends HttpAction
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(Request $request)
    {
        try{
            $this->commandBus->dispatch(
                new RegisterResetPassword([
                    'email' => $request->input('email')
                ])
            );
        }catch (MessageDispatchException $exception){
            return $this->failureResponse(
                $this->handleExceptionMessage($exception)
            );
        }

        return $this->successResponse(
            'Check your inbox and follow instruction to reset your password'
        );
    }
}