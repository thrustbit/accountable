<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Http\Actions\Account;

use Illuminate\Http\Request;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Prooph\ServiceBus\QueryBus;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\Accountable\Application\Http\Actions\HttpAction;
use Thrustbit\Accountable\Domain\Account\LocalUser;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Command\UserResetPassword;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Query\GetUserByPasswordResetToken;

class ResetPasswordWithTokenAction extends HttpAction
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var QueryBus
     */
    private $queryBus;

    public function __construct(CommandBus $commandBus, QueryBus $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }

    public function __invoke(Request $request, string $token): Response
    {
        $user = $this->getUserByToken($token);

        if (!$user || ! $user instanceof LocalUser) {
            return $this->failureResponse(
                'Reset token is not found, invalid or has been expired'
            );
        }

        try {
            $this->commandBus->dispatch(
                new UserResetPassword([
                    'user_id' => $user->getId()->identify(),
                    'password_reset_token' => $token,
                    'new_password' => $request->input('new_password'),
                    'new_password_confirmation' => $request->input('new_password_confirmation'),
                ])
            );
        } catch (MessageDispatchException $exception) {
            return $this->failureResponse(
                $this->handleExceptionMessage($exception)
            );
        }

        return $this->successResponse('Password updated successfully');
    }

    private function getUserByToken(string $resetToken)
    {
        return $this->fromPromise(
            $this->queryBus->dispatch(
                new GetUserByPasswordResetToken(['password_reset_token' => $resetToken])
            )
        );
    }
}