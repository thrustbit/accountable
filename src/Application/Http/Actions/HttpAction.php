<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Http\Actions;

use Illuminate\Http\JsonResponse;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use React\Promise\PromiseInterface;
use Symfony\Component\HttpFoundation\Response;
use Thrustbit\DevDomain\Application\Exceptions\DomainException;

class HttpAction
{
    protected function buildResponse(string $message, int $statusCode, array $extras = []): Response
    {
        return new JsonResponse([
            'data' => [
                'message' => $message,
                'code' => $statusCode,
                'extra' => $extras
            ]
        ]);
    }

    protected function successResponse(string $message, int $statusCode = 302, array $extras = []): Response
    {
        return $this->buildResponse($message, $statusCode, $extras);
    }

    protected function failureResponse(string $message, int $statusCode = 423, array $extras = []): Response
    {
        return $this->buildResponse($message, $statusCode, $extras);
    }

    protected function fromPromise(PromiseInterface $promise)
    {
        $result = null;
        $exception = null;

        $promise
            ->then(
                function ($data) use (&$result) {
                    $result = $data;
                }, function (MessageDispatchException $e) use (&$exception) {
                $exception = $e;
            });

        return $exception ?? $result;
    }

    protected function handleExceptionMessage(MessageDispatchException $exception): string
    {
        $previousException = $exception->getPrevious();

        if (!$previousException) {
            return 'An error occurred ... Try again!';
        }

        if (!$previousException instanceof DomainException) {
            throw $previousException;
        }

        return $exception->getPrevious()->getMessage();
    }
}