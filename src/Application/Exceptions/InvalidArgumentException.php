<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Exceptions;

use Thrustbit\DevDomain\Application\Exceptions\DomainException;

class InvalidArgumentException extends DomainException
{
    public static function unknownIdentifier($identifier): InvalidArgumentException
    {
        $identifierType = is_object($identifier) ? get_class($identifier) : gettype($identifier);

        return new self(sprintf('Identifier type is not supported: %s', $identifierType));
    }
}