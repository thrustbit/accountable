<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Exceptions;

class InvalidOperation extends InvalidArgumentException
{
    public static function reason(string $message, ... $fields): InvalidOperation
    {
        return new self(sprintf($message, $fields));
    }
}