<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * @var string
     */
    protected $namespace = 'Thrustbit\Accountable\Application\Http';

    public function boot()
    {
        parent::boot();
    }

    public function map(): void
    {
        Route::middleware('query_api')
            ->prefix('api/v1')
            ->group(__DIR__ . '/../../../routes/query_api.php');

        Route::middleware('command_api')
            ->prefix('api/v1')
            ->group(__DIR__ . '/../../../routes/command_api.php');
    }
}