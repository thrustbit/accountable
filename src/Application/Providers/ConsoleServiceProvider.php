<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Providers;

use Illuminate\Support\ServiceProvider;
use Thrustbit\Accountable\Application\Console\CreateRoleCommand;
use Thrustbit\Accountable\Application\Console\TestAppCommand;

class ConsoleServiceProvider extends ServiceProvider
{
    protected $commands = [
        CreateRoleCommand::class,
        TestAppCommand::class
    ];

    public function boot()
    {
        $this->commands($this->commands);
    }
}