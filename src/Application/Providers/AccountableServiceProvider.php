<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Providers;

use Illuminate\Support\AggregateServiceProvider;
use Thrustbit\DevApi\Http\Providers\DevApiServiceProvider;
use Thrustbit\ModelEvent\ModelEventServiceProvider;

class AccountableServiceProvider extends AggregateServiceProvider
{
    protected $providers = [

        // Dev api pck
        DevApiServiceProvider::class,

        // Model event pck
        ModelEventServiceProvider::class,

        // App
        MigrationServiceProvider::class,
        RouteServiceProvider::class,
        ConsoleServiceProvider::class,


        ServiceBusServiceProvider::class,
        ServicesServiceProvider::class,
        RepositoryServiceProvider::class
    ];
}