<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Providers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Thrustbit\Accountable\Domain\Account\LocalUser;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\PasswordReset;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Repository\ResetPasswordCollection;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Repository\ResetPasswordRead;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Activation;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Repository\ActivationCollection;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Repository\ActivationRead;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\UserActivation;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Repository\ThrottleCollection;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Repository\ThrottleRead;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Throttle;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\UserThrottle;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Enrole;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Repository\EnroleCollection;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Repository\EnroleRead;
use Thrustbit\Accountable\Domain\Account\Repository\UserCollection;
use Thrustbit\Accountable\Domain\Account\Repository\UserRead;
use Thrustbit\Accountable\Domain\Account\User;
use Thrustbit\Accountable\Domain\Role\Repository\RoleList;
use Thrustbit\Accountable\Domain\Role\Repository\RoleRead;
use Thrustbit\Accountable\Domain\Role\Role;
use Thrustbit\Accountable\Domain\Role\RoleContract;
use Thrustbit\Accountable\Infrastructure\Repository\ActivationRepository;
use Thrustbit\Accountable\Infrastructure\Repository\EnroleRepository;
use Thrustbit\Accountable\Infrastructure\Repository\PasswordResetRepository;
use Thrustbit\Accountable\Infrastructure\Repository\RoleRepository;
use Thrustbit\Accountable\Infrastructure\Repository\ThrottleRepository;
use Thrustbit\Accountable\Infrastructure\Repository\UserRepository;
use Thrustbit\Accountable\Projection\Credential\PasswordReset\PasswordResetFinder;
use Thrustbit\Accountable\Projection\Enabler\Activation\ActivationFinder;
use Thrustbit\Accountable\Projection\Enabler\Throttle\ThrottleFinder;
use Thrustbit\Accountable\Projection\Enrole\EnroleFinder;
use Thrustbit\Accountable\Projection\Role\RoleCacheFinder;
use Thrustbit\Accountable\Projection\User\UserFinder;
use Thrustbit\ModelEvent\EventLog\EventLog;
use Thrustbit\ModelEvent\EventLog\EventLogger;
use Thrustbit\ModelEvent\EventLog\Stream\StreamName;
use Thrustbit\ModelEvent\EventLog\TransactionalEventLogger;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * @var array
     */
    protected $binders = [

        RoleList::class => [
            'model' => [RoleContract::class, Role::class,],
            'read_model' => [RoleRead::class, RoleCacheFinder::class],
            'stream_name' => 'role-stream',
            'repository_class' => RoleRepository::class
        ],

        UserCollection::class => [
            'model' => [LocalUser::class, User::class,],
            'read_model' => [UserRead::class, UserFinder::class],
            'stream_name' => 'user-stream',
            'repository_class' => UserRepository::class
        ],

        ActivationCollection::class => [
            'model' => [UserActivation::class, Activation::class,],
            'read_model' => [ActivationRead::class, ActivationFinder::class],
            'stream_name' => 'user_activation-stream',
            'repository_class' => ActivationRepository::class
        ],

        ThrottleCollection::class => [
            'model' => [UserThrottle::class, Throttle::class,],
            'read_model' => [ThrottleRead::class, ThrottleFinder::class],
            'stream_name' => 'user_throttle-stream',
            'repository_class' => ThrottleRepository::class
        ],

        EnroleCollection::class => [
            'model' => [Enrole::class, Enrole::class,],
            'read_model' => [EnroleRead::class, EnroleFinder::class],
            'stream_name' => 'enrole-stream',
            'repository_class' => EnroleRepository::class
        ],

        ResetPasswordCollection::class => [
            'model' => [PasswordReset::class, PasswordReset::class,],
            'read_model' => [ResetPasswordRead::class, PasswordResetFinder::class],
            'stream_name' => 'user_reset_password-stream',
            'repository_class' => PasswordResetRepository::class
        ],
    ];

    public function register(): void
    {
        $this->app->singleton(EventLogger::class, EventLog::class);
        $this->app->alias(EventLogger::class, TransactionalEventLogger::class);

        foreach ($this->binders as $key => $attributes) {

            [$modelAbstract, $modelConcrete] = $attributes['model'];
            $this->app->bind($modelAbstract, $modelConcrete);

            [$readModelAbstract, $readModelConcrete] = $attributes['read_model'];
            $this->app->bind($readModelAbstract, $readModelConcrete);

            $this->app->bind($key, function (Application $app) use ($attributes, $readModelAbstract) {

                return new $attributes['repository_class'](
                    $app->make(EventLog::class),
                    $app->make($readModelAbstract),
                    new StreamName($attributes['stream_name'])
                );
            });
        }
    }

    public function provides(): array
    {
        $provides = [EventLogger::class, TransactionalEventLogger::class];

        foreach ($this->binders as $collection => $keys) {
            [$model] = $keys['model'];
            [$readModel] = $keys['read_model'];
            $provides += [$collection, $readModel, $model];
        }

        return $provides;
    }
}