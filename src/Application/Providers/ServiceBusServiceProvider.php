<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Providers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\EventBus;
use Prooph\ServiceBus\QueryBus;
use Thrustbit\Accountable\Application\ServiceBus\ApiMessage;
use Thrustbit\Accountable\Application\ServiceBus\ApiRespondToException;
use Thrustbit\DevApi\Http\Response\ApiResponse;
use Thrustbit\ServiceBus\BusDispatcher;
use Thrustbit\ServiceBus\CommandBusManager;
use Thrustbit\ServiceBus\EventBusManager;
use Thrustbit\ServiceBus\QueryBusManager;
use Thrustbit\ServiceBus\ServiceBus;

class ServiceBusServiceProvider extends ServiceProvider
{
    protected $defer = true;

    protected $managers = [CommandBusManager::class, EventBusManager::class, QueryBusManager::class];

    protected $bus = [
        'command' => [
            ['command_bus', CommandBus::class]
        ],
        'event' => [
            ['event_bus', EventBus::class]
        ],
        'query' => [
            ['query_bus', QueryBus::class]
        ]
    ];

    public function boot()
    {
        $this->registerBus();

        $this->registerBusRoutes();
    }

    protected function registerBus(): void
    {
        foreach ($this->bus as $bus => $message) {
            foreach ($message as [$alias, $concrete]) {

                switch ($bus) {
                    case 'command':
                        $this->app->singleton($alias, function (Application $app) use ($alias) {
                            return $app[CommandBusManager::class]->command($alias);
                        });
                        break;

                    case 'event':
                        $this->app->singleton($alias, function (Application $app) use ($alias) {
                            return $app[EventBusManager::class]->event($alias);
                        });
                        break;

                    case 'query':
                        $this->app->singleton($alias, function (Application $app) use ($alias) {
                            return $app[QueryBusManager::class]->query($alias);
                        });
                        break;
                }

                $this->app->alias($alias, $concrete);
            }
        }
    }

    protected function registerBusRoutes(): void
    {
        $commandRoutes = config('laraprooph.laraprooph.router.command_bus.router.routes');
        $eventRoutes = config('laraprooph.laraprooph.router.event_bus.router.routes');
        $queryRoutes = config('laraprooph.laraprooph.router.query_bus.router.routes');

        foreach ([$commandRoutes, $eventRoutes, $queryRoutes] as $routes) {
            foreach ($routes as $command => $handler) {
                $this->app->bindIf($command);

                if (is_array($handler)) {
                    foreach ($handler as $_handler) {
                        $this->app->bindIf($_handler);
                    }
                } else {
                    $this->app->bindIf($handler);
                }
            }
        }
    }

    public function register(): void
    {
        $this->registerServiceBusManager();
    }

    private function registerServiceBusManager(): void
    {
        $this->registerConfiguration();

        $this->registerManagers();

        $this->registerServiceBus();
    }

    protected function registerConfiguration(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../../../config/service_bus.php', 'laraprooph');

        // $this->mergeConfigFrom(__DIR__ . '/../../../config/dev_api.php', 'dev_api');
    }

    protected function registerManagers(): void
    {
        foreach ($this->managers as $manager) {
            $this->app->singleton($manager);
        }
    }

    public function provides(): array
    {
        return $this->managers;
    }

    protected function registerServiceBus()
    {
        $this->app->bind(ServiceBus::class, BusDispatcher::class);

        $this->app->bind(ApiResponse::class, ApiRespondToException::class);
    }
}