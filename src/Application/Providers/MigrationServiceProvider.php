<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Providers;

use Illuminate\Support\ServiceProvider;

class MigrationServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/../../../database');
    }
}