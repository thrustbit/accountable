<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Application\Providers;

use Illuminate\Support\ServiceProvider;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Services\FirstActiveResetTokenMatcher;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Services\ActivationTokenAvailability;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Services\UserRoleAvailable;
use Thrustbit\Accountable\Domain\Account\Services\EmailAvailability;
use Thrustbit\Accountable\Domain\Account\Services\PasswordMaker;
use Thrustbit\Accountable\Domain\Account\Services\UserNameAvailability;
use Thrustbit\Accountable\Domain\Role\Services\RoleNameAvailability;
use Thrustbit\Accountable\Infrastructure\Services\ActivationTokenUnique;
use Thrustbit\Accountable\Infrastructure\Services\BcryptPasswordMaker;
use Thrustbit\Accountable\Infrastructure\Services\EmailAvailable;
use Thrustbit\Accountable\Infrastructure\Services\MatchFirstActiveResetToken;
use Thrustbit\Accountable\Infrastructure\Services\RoleNameAvailable;
use Thrustbit\Accountable\Infrastructure\Services\UserNameAvailable;
use Thrustbit\Accountable\Infrastructure\Services\UserRoleUnique;

class ServicesServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * @var array
     */
    protected $services = [
        RoleNameAvailability::class => RoleNameAvailable::class,
        EmailAvailability::class => EmailAvailable::class,
        UserNameAvailability::class => UserNameAvailable::class,
        PasswordMaker::class => BcryptPasswordMaker::class,
        UserRoleAvailable::class => UserRoleUnique::class,
        FirstActiveResetTokenMatcher::class => MatchFirstActiveResetToken::class,
        ActivationTokenAvailability::class => ActivationTokenUnique::class,
    ];

    public function register(): void
    {
        foreach ($this->services as $abstract => $concrete) {
            $this->app->bindIf($abstract, $concrete);
        }
    }

    public function provides(): array
    {
        return array_keys($this->services);
    }
}