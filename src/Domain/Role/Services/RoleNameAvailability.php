<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Role\Services;

use Thrustbit\Accountable\Domain\Role\Values\RoleId;
use Thrustbit\Accountable\Domain\Role\Values\RoleName;

interface RoleNameAvailability
{
    public function __invoke(RoleName $roleName): ?RoleId;
}