<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Role;

use Thrustbit\Accountable\Domain\Role\Values\RoleName;

interface RoleContract
{
    public function getName(): RoleName;

    public function __toString(): string;
}