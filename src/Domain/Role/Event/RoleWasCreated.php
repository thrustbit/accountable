<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Role\Event;

use Thrustbit\ModelEvent\ModelChanged;

class RoleWasCreated extends ModelChanged
{
}