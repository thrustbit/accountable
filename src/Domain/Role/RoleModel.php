<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Role;

use Thrustbit\Accountable\Domain\Role\Values\RoleId;
use Thrustbit\Accountable\Domain\Role\Values\RoleName;
use Thrustbit\DevDomain\Application\Values\Entity;
use Thrustbit\ModelEvent\ModelRoot;

abstract class RoleModel extends ModelRoot implements RoleContract, Entity
{

    /**
     * @var string
     */
    protected $table = 'roles';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'slug', 'description',
    ];

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var bool
     */
    public $incrementing = false;

    public function getId(): RoleId
    {
        return $this->getKey() instanceof RoleId
            ? $this->getKey()
            : RoleId::fromString($this->getKey());
    }

    public function getName(): RoleName
    {
        return $this['name'] instanceof RoleName
            ? $this['name']
            : RoleName::fromString($this['name']);
    }

    public function getSlug(): string
    {
        return $this->getName()->getSlug();
    }

    public function getDescription(): string
    {
        return $this['description'];
    }

    protected function aggregateId(): string
    {
        return $this->getId()->identify();
    }

    public function __toString(): string
    {
        return parent::__toString();
    }
}