<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Role\Handler;

use Thrustbit\Accountable\Domain\Role\Command\CreateRole;
use Thrustbit\Accountable\Domain\Role\Exceptions\RoleAlreadyExists;
use Thrustbit\Accountable\Domain\Role\Repository\RoleList;
use Thrustbit\Accountable\Domain\Role\Role;
use Thrustbit\Accountable\Domain\Role\Services\RoleNameAvailability;

class CreateRoleHandler
{
    /**
     * @var RoleList
     */
    private $roleList;

    /**
     * @var RoleNameAvailability
     */
    private $uniqueRoleName;

    public function __construct(RoleList $roleList, RoleNameAvailability $uniqueRoleName)
    {
        $this->roleList = $roleList;
        $this->uniqueRoleName = $uniqueRoleName;
    }

    public function __invoke(CreateRole $command): void
    {
        if (null !== $roleId = ($this->uniqueRoleName)($command->roleName())) {
            throw RoleAlreadyExists::withRoleIdentifier($command->roleName(), $roleId);
        }

        $role = Role::createRole($command->roleId(), $command->roleName(), $command->roleDescription());

        $this->roleList->save($role);
    }
}