<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Role\Repository;

use Thrustbit\Accountable\Domain\Role\Role;
use Thrustbit\DevDomain\Application\Values\Identifier;
use Thrustbit\ModelEvent\Model\Repository\RepositoryRead;

interface RoleRead extends RepositoryRead
{
    public function roleOfId(string $roleId): ?Role;

    public function roleOfName(string $roleName): ?Role;

    public function roleOfIdentifier(Identifier $identifier): ?Role;
}