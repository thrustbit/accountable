<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Role\Repository;

use Thrustbit\Accountable\Domain\Role\Role;

interface RoleList
{
    public function get(string $roleId): ?Role;

    public function save(Role $role): void;
}