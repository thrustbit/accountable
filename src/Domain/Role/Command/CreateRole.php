<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Role\Command;

use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Thrustbit\Accountable\Domain\Role\Values\RoleId;
use Thrustbit\Accountable\Domain\Role\Values\RoleName;
use Thrustbit\DevDomain\Application\Exceptions\Validate;

class CreateRole extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public function roleId(): RoleId
    {
        return RoleId::fromString($this->payload()['role_id']);
    }

    public function roleName(): RoleName
    {
        return RoleName::fromString($this->payload()['role_name']);
    }

    public function roleDescription(): string
    {
        $description = $this->payload()['role_description'];

        Validate::string($description);
        Validate::notEmpty($description);

        return $description;
    }
}