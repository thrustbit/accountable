<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Role\Exceptions;

use Thrustbit\Accountable\Domain\Role\Values\RoleId;
use Thrustbit\Accountable\Domain\Role\Values\RoleName;

class RoleAlreadyExists extends RoleException
{
    public static function withRoleIdentifier(RoleName $roleName, RoleId $roleId): RoleAlreadyExists
    {
        return new self(
            sprintf('Role with name %s and id %s already exists.', $roleName->identify(), $roleId->identify())
        );
    }
}