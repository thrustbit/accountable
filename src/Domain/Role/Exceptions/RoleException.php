<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Role\Exceptions;

use Thrustbit\DevDomain\Application\Exceptions\DomainException;

class RoleException extends DomainException
{

}