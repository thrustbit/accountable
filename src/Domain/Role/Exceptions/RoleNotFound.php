<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Role\Exceptions;

use Thrustbit\DevDomain\Application\Values\Identifier;

class RoleNotFound extends RoleException
{
    public static function withIdentifier(Identifier $identifier): RoleNotFound
    {
        return new self(
            sprintf('Role not found with identifier %s', $identifier->identify())
        );
    }
}