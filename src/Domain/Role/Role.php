<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Role;

use Thrustbit\Accountable\Domain\Role\Event\RoleWasCreated;
use Thrustbit\Accountable\Domain\Role\Values\RoleId;
use Thrustbit\Accountable\Domain\Role\Values\RoleName;
use Thrustbit\DevDomain\Application\Values\Entity;

final class Role extends RoleModel
{
    public static function createRole(RoleId $roleId, RoleName $roleName, string $description): self
    {
        $self = new self();
        $self->recordThat(RoleWasCreated::occur($roleId->identify(), [
            'role_name' => $roleName->getName(),
            'role_slug' => $roleName->getSlug(),
            'role_description' => $description
        ]));

        return $self;
    }

    public function sameIdentityAs(Entity $aEntity): bool
    {
        return $aEntity instanceof $this &&
            $this->getId()->sameValueAs($aEntity->getId());
    }

    protected function whenRoleWasCreated(RoleWasCreated $event): void
    {
        $this['id'] = $event->aggregateId();
        $this['name'] = $event->payload()['role_name'];
        $this['slug'] = $event->payload()['role_slug'];
        $this['description'] = $event->payload()['role_description'];

        // Fix slug as it depends w/RoleName vo
    }
}