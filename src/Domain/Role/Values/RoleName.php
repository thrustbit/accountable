<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Role\Values;

use Thrustbit\DevDomain\Application\Exceptions\Validate;
use Thrustbit\DevDomain\Application\Values\Identifier;
use Thrustbit\DevDomain\Application\Values\Value;

class RoleName implements Identifier
{

    const PREFIX = 'ROLE_';

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    private function __construct(string $name)
    {
        $this->name = $name;
        $this->slug = str_slug($name);
    }

    public static function fromString($roleName): self
    {
        Validate::string($roleName);
        Validate::notEmpty($roleName);

        $roleName = strtoupper($roleName);

        Validate::startsWith($roleName, self::PREFIX);

        Validate::betweenLength($roleName, 7, 255);

        return new self($roleName);
    }

    public function identify(): string
    {
        return $this->name;
    }

    public function toString(): string
    {
        return $this->name;
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this && $this->name === $aValue->toString();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}