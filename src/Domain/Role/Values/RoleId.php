<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Role\Values;

use Ramsey\Uuid\UuidInterface;
use Thrustbit\DevDomain\Application\Exceptions\Validate;
use Thrustbit\DevDomain\Application\Values\Contracts\Uuid;
use Thrustbit\DevDomain\Application\Values\Value;

class RoleId implements Uuid
{

    /**
     * @var UuidInterface
     */
    private $uid;

    private function __construct(UuidInterface $uid)
    {
        $this->uid = $uid;
    }

    public static function nextIdentity(): self
    {
        return new self(\Ramsey\Uuid\Uuid::uuid4());
    }

    public static function fromString($roleId): self
    {
        $message = 'Role id is not valid';

        Validate::string($roleId, $message);
        Validate::notEmpty($roleId, $message);
        Validate::uuid($roleId, $message);

        return new self(\Ramsey\Uuid\Uuid::fromString($roleId));
    }

    public function identify(): string
    {
        return $this->uid->toString();
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof RoleId
            && $this->uid->equals($aValue->getUid());
    }

    public function getUid(): UuidInterface
    {
        return $this->uid;
    }

    public function __toString(): string
    {
        return $this->uid->toString();
    }
}