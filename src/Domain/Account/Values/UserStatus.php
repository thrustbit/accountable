<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Values;

use Thrustbit\DevDomain\Application\Values\Enum;

/**
 * @method static UserStatus PENDING_REGISTRATION()
 * @method static UserStatus NOT_ACTIVATED()
 * @method static UserStatus ACTIVATED()
 * @method static UserStatus SUSPENDED()
 * @method static UserStatus BANNED()
 */
class UserStatus extends Enum
{
    const PENDING_REGISTRATION = 501;

    const NOT_ACTIVATED = 401;

    const ACTIVATED = 302;

    const SUSPENDED = 403;

    const BANNED = 423;

    /**
     * @var array
     */
    public static $statusTexts = [
        501 => 'Account registration is not yet processed',
        302 => 'Account has benn successfully activated',
        401 => 'Account is not yet activated',
        403 => 'Account has been suspended',
        423 => 'Account has been banned'
    ];

    public function isNotPending(): bool
    {
        return self::PENDING_REGISTRATION !== $this->getValue();
    }

    public function isActivated(): bool
    {
        return self::ACTIVATED === $this->getValue();
    }

    public function isNotActivated(): bool
    {
        return self::NOT_ACTIVATED !== $this->getValue();
    }

    public function isNotSuspended(): bool
    {
        return self::SUSPENDED !== $this->getValue();
    }

    public function isNotBanned(): bool
    {
        return self::BANNED !== $this->getValue();
    }

    public function isNonLocked(): bool
    {
        return $this->isNotBanned() && $this->isNotSuspended();
    }

    public function isEnabled(): bool
    {
        return $this->isActivated() && $this->isNonLocked() && $this->isNotPending();
    }

    public function getMessageByStatus(): string
    {
        return static::$statusTexts[$this->getValue()];
    }
}