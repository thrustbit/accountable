<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Exceptions;

use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class RegistrationStillPending extends UserStatusException
{
    public static function withUserId(UserId $userId): RegistrationStillPending
    {
        return new self(sprintf('Registration still pending with user id %s', $userId->identify()));
    }
}