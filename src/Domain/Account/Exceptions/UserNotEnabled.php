<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Exceptions;

use Thrustbit\Accountable\Domain\Account\Values\UserStatus;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class UserNotEnabled extends UserStatusException
{
    public static function withUser(UserId $userId, UserStatus $currentStatus): self
    {
        return new self(
            sprintf('User with id %s is not enabled, current status is %s',
                $userId->identify(), $currentStatus->getMessageByStatus())
        );
    }
}