<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Exceptions;

use Thrustbit\DevDomain\Application\Values\Identifier;

class UserNotFound extends UserException
{
    public static function withIdentifier(Identifier $identifier): UserNotFound
    {
        return new self(
            sprintf('User not found with identifier %s', $identifier->identify())
        );
    }
}