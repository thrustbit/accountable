<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Exceptions;

use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;

class EmailAlreadyExists extends UserException
{
    public static function withEmail(EmailAddress $email): self
    {
        return new self(sprintf('User with email %s already exists.', $email->identify()));
    }
}