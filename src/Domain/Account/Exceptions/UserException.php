<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Exceptions;

use Thrustbit\DevDomain\Application\Exceptions\DomainException;

class UserException extends DomainException
{
}