<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Exceptions;

use Thrustbit\DevDomain\Application\Values\UserName\UserName;

class UserNameAlreadyExists extends UserException
{
    public static function withUserName(UserName $userName): self
    {
        return new self(sprintf('User with name %s already exists.', $userName->identify()));
    }
}