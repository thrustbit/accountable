<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Exceptions;

use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class BadCredentials extends UserException
{
    public static function withUserId(UserId $userId): BadCredentials
    {
        return new self(sprintf('Invalid credentials for user id %s', $userId->identify()));
    }
}