<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account;

use Thrustbit\DevDomain\Application\Values\Contracts\EncodedPassword;

interface LocalUser extends Accountable
{
    public function getPassword(): EncodedPassword;
}