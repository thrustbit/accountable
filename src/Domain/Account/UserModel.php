<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Thrustbit\Accountable\Domain\Account\Exceptions\UserNotEnabled;
use Thrustbit\Accountable\Domain\Account\Values\UserStatus;
use Thrustbit\Accountable\Domain\Role\Role;
use Thrustbit\Accountable\Projection\AccountableStore;
use Thrustbit\DevDomain\Application\Values\Contracts\EncodedPassword;
use Thrustbit\DevDomain\Application\Values\Credentials\BcryptPassword;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;
use Thrustbit\DevDomain\Application\Values\Entity;
use Thrustbit\DevDomain\Application\Values\Identifier;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;
use Thrustbit\DevDomain\Application\Values\UserName\UserName;
use Thrustbit\ModelEvent\ModelRoot;

abstract class UserModel extends ModelRoot implements LocalUser, Entity
{
    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'user_name', 'email', 'password', 'status'
    ];

    /**
     * @var array
     */
    protected $hidden = ['password'];

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var bool
     */
    public $incrementing = false;

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, AccountableStore::ROLE_USER_TABLE, 'user_id');
    }

    public function getId(): UserId
    {
        return $this->getKey() instanceof UserId
            ? $this->getKey()
            : UserId::fromString($this->getKey());
    }

    public function getIdentifier(): Identifier
    {
        return $this->getId();
    }

    public function getUserName(): UserName
    {
        return $this['user_name'] instanceof UserName
            ? $this['user_name']
            : UserName::fromString($this['user_name']);
    }

    public function getEmail(): EmailAddress
    {
        return $this['email'] instanceof EmailAddress
            ? $this['email']
            : EmailAddress::fromString($this['email']);
    }

    public function getRoles(): array
    {
        if (!$this->relationLoaded('roles')) {
            $this->load('roles');
        }

        return $this->getRelation(('roles'))->pluck('name')->toArray();
    }

    public function getPassword(): EncodedPassword
    {
        return $this['password'] instanceof EncodedPassword
            ? $this['password']
            : BcryptPassword::fromString($this['password']);
    }

    public function getUserStatus(): UserStatus
    {
        return UserStatus::byValue($this['status']);
    }

    protected function aggregateId(): string
    {
        return $this->getId()->identify();
    }

    protected function requireEnabledUser(): void
    {
        if (!$this->getUserStatus()->isEnabled()) {
            throw UserNotEnabled::withUser($this->getId(), $this->getUserStatus());
        }
    }
}