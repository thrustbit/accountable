<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Handler;

use Thrustbit\Accountable\Domain\Account\Command\ChangeEmail;
use Thrustbit\Accountable\Domain\Account\Exceptions\EmailAlreadyExists;
use Thrustbit\Accountable\Domain\Account\Exceptions\UserNotFound;
use Thrustbit\Accountable\Domain\Account\Repository\UserCollection;
use Thrustbit\Accountable\Domain\Account\Services\EmailAvailability;

class ChangeEmailHandler
{
    /**
     * @var UserCollection
     */
    private $userCollection;
    /**
     * @var EmailAvailability
     */
    private $isEmailUnique;

    public function __construct(UserCollection $userCollection, EmailAvailability $isEmailUnique)
    {
        $this->userCollection = $userCollection;
        $this->isEmailUnique = $isEmailUnique;
    }

    public function __invoke(ChangeEmail $command): void
    {
        $userId = $command->userId();
        $user = $this->userCollection->get($userId->identify());

        if (!$user) {
            throw UserNotFound::withIdentifier($userId);
        }

        $newEmail = $command->newEmail();

        if (null !== ($this->isEmailUnique)($newEmail)) {
            throw EmailAlreadyExists::withEmail($newEmail);
        }

        $user->changeEmail($newEmail);

        $this->userCollection->save($user);
    }
}