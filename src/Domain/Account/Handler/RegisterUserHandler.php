<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Handler;

use Thrustbit\Accountable\Domain\Account\Command\RegisterUser;
use Thrustbit\Accountable\Domain\Account\Repository\UserCollection;
use Thrustbit\Accountable\Domain\Account\Services\PasswordMaker;
use Thrustbit\Accountable\Domain\Account\Services\UserRegistrationValidator;
use Thrustbit\Accountable\Domain\Account\User;

class RegisterUserHandler
{

    /**
     * @var UserCollection
     */
    private $userCollection;

    /**
     * @var UserRegistrationValidator
     */
    private $validator;

    /**
     * @var PasswordMaker
     */
    private $passwordMaker;

    public function __construct(UserCollection $userCollection,
                                UserRegistrationValidator $validator,
                                PasswordMaker $passwordMaker)
    {
        $this->userCollection = $userCollection;
        $this->validator = $validator;
        $this->passwordMaker = $passwordMaker;
    }

    public function __invoke(RegisterUser $command): void
    {
        ($this->validator)($command); //fixMe

        $encodedPassword = ($this->passwordMaker)($command->password());

        $user = User::registerUser($command->userId(), $command->email(), $command->userName(), $encodedPassword);

        $this->userCollection->save($user);
    }
}