<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Handler;

use React\Promise\Deferred;
use Thrustbit\Accountable\Domain\Account\Query\GetUserByEmail;
use Thrustbit\Accountable\Domain\Account\Repository\UserRead;

class GetUserByEmailHandler
{
    /**
     * @var UserRead
     */
    private $userRead;

    public function __construct(UserRead $userRead)
    {
        $this->userRead = $userRead;
    }

    public function __invoke(GetUserByEmail $query, Deferred $deferred = null)
    {
        $user = $this->userRead->userOfEmail($query->email()->identify());

        if (!$deferred) {
            return $user;
        }

        $deferred->resolve($user);
    }
}