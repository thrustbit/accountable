<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Handler;

use Thrustbit\Accountable\Domain\Account\Command\ChangePassword;
use Thrustbit\Accountable\Domain\Account\Exceptions\UserNotFound;
use Thrustbit\Accountable\Domain\Account\Repository\UserCollection;
use Thrustbit\Accountable\Domain\Account\Services\PasswordMaker;
use Thrustbit\Accountable\Domain\Account\Values\UserPassword;

class ChangePasswordHandler
{
    /**
     * @var UserCollection
     */
    private $userCollection;

    /**
     * @var PasswordMaker
     */
    private $passwordMaker;

    public function __construct(UserCollection $userCollection, PasswordMaker $passwordMaker)
    {
        $this->userCollection = $userCollection;
        $this->passwordMaker = $passwordMaker;
    }

    public function __invoke(ChangePassword $command): void
    {
        $user = $this->userCollection->get($command->userId()->identify());

        if (!$user) {
            throw UserNotFound::withIdentifier($command->userId());
        }

        $user->changePassword($command->currentPassword(), $command->newPassword(), $this->passwordMaker);

        $this->userCollection->save($user);
    }
}