<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Handler;

use React\Promise\Deferred;
use Thrustbit\Accountable\Domain\Account\Query\GetUsers;
use Thrustbit\Accountable\Domain\Account\Repository\UserRead;

class GetUsersHandler
{
    /**
     * @var UserRead
     */
    private $userRead;

    public function __construct(UserRead $userRead)
    {
        $this->userRead = $userRead;
    }

    public function __invoke(GetUsers $query, Deferred $deferred = null)
    {
        $users = $this->userRead->all();

        if (!$deferred) {
            return $users;
        }

        $deferred->resolve($users);
    }
}