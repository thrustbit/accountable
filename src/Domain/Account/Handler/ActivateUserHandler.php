<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Handler;

use Thrustbit\Accountable\Domain\Account\Command\ActivateUser;
use Thrustbit\Accountable\Domain\Account\Exceptions\UserNotFound;
use Thrustbit\Accountable\Domain\Account\Repository\UserCollection;

class ActivateUserHandler
{
    /**
     * @var UserCollection
     */
    private $userCollection;

    public function __construct(UserCollection $userCollection)
    {
        $this->userCollection = $userCollection;
    }

    public function __invoke(ActivateUser $command): void
    {
        $user = $this->userCollection->get($command->userId()->identify());

        if (!$user) {
            throw UserNotFound::withIdentifier($command->userId());
        }

        $user->activateUser();

        $this->userCollection->save($user);
    }
}