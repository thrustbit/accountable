<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Handler;

use React\Promise\Deferred;
use Thrustbit\Accountable\Domain\Account\Query\GetUserById;
use Thrustbit\Accountable\Domain\Account\Repository\UserCollection;

class GetUserByIdHandler
{
    /**
     * @var UserCollection
     */
    private $userCollection;

    public function __construct(UserCollection $userCollection)
    {
        $this->userCollection = $userCollection;
    }

    public function __invoke(GetUserById $query, Deferred $deferred = null)
    {
        $user = $this->userCollection->get($query->userId()->identify());

        if (!$deferred) {
            return $user;
        }

        $deferred->resolve($user);
    }
}