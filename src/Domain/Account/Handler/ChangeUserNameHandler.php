<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Handler;

use Thrustbit\Accountable\Domain\Account\Command\ChangeUserName;
use Thrustbit\Accountable\Domain\Account\Exceptions\UserNotFound;
use Thrustbit\Accountable\Domain\Account\Repository\UserCollection;
use Thrustbit\Accountable\Domain\Account\Services\UserNameAvailability;

class ChangeUserNameHandler
{
    /**
     * @var UserCollection
     */
    private $userCollection;

    /**
     * @var UniqueUserName
     */
    private $uniqueUserName;

    public function __construct(UserCollection $userCollection, UserNameAvailability $uniqueUserName)
    {
        $this->userCollection = $userCollection;
        $this->uniqueUserName = $uniqueUserName;
    }

    public function __invoke(ChangeUserName $command): void
    {
        $userId = $command->userId();
        $newUserName = $command->newUserName();

        $user = $this->userCollection->get($userId->identify());

        if(!$user){
            throw UserNotFound::withIdentifier($userId);
        }

        $user->changeUserName($newUserName);

        $this->userCollection->save($user);
    }
}