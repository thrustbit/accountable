<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Command;

use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Values\ActivationToken;

class ActivateUserWithActivationToken extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public function activationToken(): ActivationToken
    {
        return ActivationToken::fromString($this->payload()['activation_token']);
    }
}