<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Handler;

use Thrustbit\Accountable\Domain\Account\Exceptions\UserNotFound;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Command\MarkUserAsNotActivated;
use Thrustbit\Accountable\Domain\Account\Repository\UserCollection;

class MarkUserAsNotActivatedHandler
{
    /**
     * @var UserCollection
     */
    private $userCollection;

    public function __construct(UserCollection $userCollection)
    {
        $this->userCollection = $userCollection;
    }

    public function __invoke(MarkUserAsNotActivated $command)
    {
        $identifier = $command->userId();

        $user = $this->userCollection->get($identifier->identify());

        if(!$user){
            throw UserNotFound::withIdentifier($identifier);
        }

        $user->markAsNotActivated();

        $this->userCollection->save($user);
    }
}