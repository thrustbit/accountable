<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Handler;

use Thrustbit\Accountable\Domain\Account\Model\Enabler\Command\CreateUserActivation;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Activation;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Exceptions\UserActivationException;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Repository\ActivationCollection;
use Thrustbit\Accountable\Domain\Account\Repository\UserCollection;

class CreateUserActivationHandler
{
    /**
     * @var UserCollection
     */
    private $userCollection;

    /**
     * @var ActivationCollection
     */
    private $activationCollection;

    public function __construct(UserCollection $userCollection, ActivationCollection $activationCollection)
    {
        $this->userCollection = $userCollection;
        $this->activationCollection = $activationCollection;
    }

    public function __invoke(CreateUserActivation $command): void
    {
        $userId = $command->userId();

        if ($this->activationCollection->get($userId->identify())) {
            throw new UserActivationException(
                sprintf('User activation already exits with user id %s', $userId->identify())
            );
        }

        $activation = Activation::registerUser($userId, $command->activationToken());

        $this->activationCollection->save($activation);
    }
}