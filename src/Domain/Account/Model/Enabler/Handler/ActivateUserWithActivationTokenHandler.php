<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Handler;

use Thrustbit\Accountable\Domain\Account\Exceptions\UserNotFound;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Command\ActivateUserWithActivationToken;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Exceptions\ActivationTokenNotFound;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Services\ActivationTokenAvailability;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Services\EnablerService;
use Thrustbit\Accountable\Domain\Account\Repository\UserCollection;

class ActivateUserWithActivationTokenHandler
{

    /**
     * @var UserCollection
     */
    private $userCollection;

    /**
     * @var EnablerService
     */
    private $enabler;

    /**
     * @var ActivationTokenAvailability
     */
    private $uniqueToken;

    public function __construct(UserCollection $userCollection,
                                EnablerService $enabler,
                                ActivationTokenAvailability $uniqueToken)
    {
        $this->userCollection = $userCollection;
        $this->enabler = $enabler;
        $this->uniqueToken = $uniqueToken;
    }

    public function __invoke(ActivateUserWithActivationToken $command): void
    {
        $userId = ($this->uniqueToken)($command->activationToken());

        if(!$userId){
            throw ActivationTokenNotFound::withToken($command->activationToken());
        }

        $user = $this->userCollection->get($userId->identify());

        if (!$user) {
            throw UserNotFound::withIdentifier($userId);
        }

        $this->enabler->activate($user);

        $this->userCollection->save($user);
    }
}