<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Services;

use Thrustbit\Accountable\Domain\Account\Exceptions\RegistrationStillPending;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Exceptions\UserAlreadyActivated;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Repository\ActivationCollection;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Exceptions\UserIsLocked;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Repository\ThrottleCollection;
use Thrustbit\Accountable\Domain\Account\User;

class EnablerService
{
    /**
     * @var ActivationCollection
     */
    private $activationCollection;

    /**
     * @var ThrottleCollection
     */
    private $throttleCollection;

    public function __construct(ActivationCollection $activationCollection, ThrottleCollection $throttleCollection)
    {
        $this->activationCollection = $activationCollection;
        $this->throttleCollection = $throttleCollection;
    }

    public function activate(User $user): void
    {
        $userStatus = $user->getUserStatus();

        $userId = $user->getId();

        if ($userStatus->isActivated()) {
            throw UserAlreadyActivated::withUserId($userId);
        }

        if (!$userStatus->isNotPending()) {
            throw RegistrationStillPending::withUserId($userId);
        }

        if (!$userStatus->isNonLocked()) {
            throw UserIsLocked::withUser($userId, $userStatus);
        }

        $activation = $this->activationCollection->get($userId->identify());

        $activation->activate();

        $this->activationCollection->save($activation);

        $user->activateUser();
    }

    public function suspend(User $user): void
    {

    }

    public function ban(User $user): void
    {

    }
}