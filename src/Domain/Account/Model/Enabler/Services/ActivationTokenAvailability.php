<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Services;

use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Values\ActivationToken;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

interface ActivationTokenAvailability
{
    public function __invoke(ActivationToken $activationToken): ?UserId;
}