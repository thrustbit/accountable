<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Repository;

use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Throttle;
use Thrustbit\ModelEvent\Model\Repository\RepositoryRead;

interface ThrottleRead extends RepositoryRead
{
    public function throttleOfUserId(string $userId): ?Throttle;
}