<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Repository;

use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Throttle;

interface ThrottleCollection
{
    public function get(string $userId): ?Throttle;

    public function save(Throttle $model): void;
}