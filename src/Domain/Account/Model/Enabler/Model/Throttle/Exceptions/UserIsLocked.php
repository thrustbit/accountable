<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Exceptions;

use Thrustbit\Accountable\Domain\Account\Values\UserStatus;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class UserIsLocked extends ThrottleException
{
    public static function withUser(UserId $userId, UserStatus $userStatus): UserIsLocked
    {
        return new self($userStatus->getMessageByStatus());
    }
}