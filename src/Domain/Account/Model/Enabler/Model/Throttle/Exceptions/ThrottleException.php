<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Exceptions;

use Thrustbit\Accountable\Domain\Account\Exceptions\UserStatusException;

class ThrottleException extends UserStatusException
{
}