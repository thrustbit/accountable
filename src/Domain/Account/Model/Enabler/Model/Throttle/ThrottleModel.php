<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle;

use Thrustbit\ModelEvent\ModelRoot;

abstract class ThrottleModel extends ModelRoot implements UserThrottle
{

    protected function aggregateId(): string
    {
        // TODO: Implement aggregateId() method.
    }
}