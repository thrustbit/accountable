<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Event;

use Thrustbit\ModelEvent\ModelChanged;

class UserActivationWasActivated extends ModelChanged
{
}