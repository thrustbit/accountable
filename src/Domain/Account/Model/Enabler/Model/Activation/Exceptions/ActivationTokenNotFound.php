<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Exceptions;

use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Values\ActivationToken;

class ActivationTokenNotFound extends UserActivationException
{
    public static function withToken(ActivationToken $token): ActivationTokenNotFound
    {
        return new self(sprintf('Token %s not found', $token->identify()));
    }
}