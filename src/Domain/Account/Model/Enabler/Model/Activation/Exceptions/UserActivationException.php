<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Exceptions;

use Thrustbit\Accountable\Domain\Account\Exceptions\UserStatusException;

class UserActivationException extends UserStatusException
{
}