<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Exceptions;

use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class UserAlreadyActivated extends UserActivationException
{
    public static function withUserId(UserId $userId): UserAlreadyActivated
    {
        return new self(sprintf('User with id %s already activated.', $userId->identify()));
    }
}