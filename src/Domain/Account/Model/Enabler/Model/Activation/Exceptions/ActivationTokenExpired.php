<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Exceptions;

use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Values\ActivationToken;

class ActivationTokenExpired extends ActivationTokenNotFound
{
    public static function withExpiredToken(ActivationToken $token): ActivationTokenExpired
    {
        return new self(
            sprintf('Activation token %s has been expired', $token->identify())
        );
    }
}