<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Values\ActivationStatus;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Values\ActivationToken;
use Thrustbit\Accountable\Domain\Account\User;
use Thrustbit\Accountable\Projection\AccountableStore;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;
use Thrustbit\ModelEvent\ModelRoot;

class ActivationModel extends ModelRoot implements UserActivation
{
    /**
     * @var string
     */
    protected $table = AccountableStore::USER_ACTIVATION_TABLE;

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'token', 'created_at', 'activated_at'
    ];

    /**
     * @var array
     */
    protected $hidden = ['token'];

    /**
     * @var string
     */
    protected $keyType = 'string';

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id', 'id');
    }

    public function getId(): UserId
    {
        return $this->getKey() instanceof UserId
            ? $this->getKey()
            : UserId::fromString($this->getKey());
    }

    public function getActivationToken(): ?ActivationToken
    {
        if (!$this['token']) {
            return null;
        }

        return $this['token'] instanceof ActivationToken
            ? $this['token']
            : ActivationToken::fromString($this['token']);
    }

    public function activatedAt(): ?\DateTimeImmutable
    {
        if (null === $this['activated_at']) {
            return null;
        }

        return $this['activated_at'] instanceof \DateTimeImmutable
            ? $this['activated_at']
            : new \DateTimeImmutable($this['activated_at']);
    }

    public function getActivationStatus(): ActivationStatus
    {
        return ActivationStatus::fromValues(
            $this->createdAt(),
            $this->getActivationToken(),
            $this->activatedAt()
        );
    }

    protected function aggregateId(): string
    {
        return $this->getId()->identify();
    }
}