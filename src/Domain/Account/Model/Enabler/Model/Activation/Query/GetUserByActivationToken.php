<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Query;

use Prooph\Common\Messaging\DomainEvent;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Values\ActivationToken;

class GetUserByActivationToken extends DomainEvent implements PayloadConstructable
{
    use PayloadTrait;

    public function activationToken(): ActivationToken
    {
        return ActivationToken::fromString($this->payload()['activation_token']);
    }
}