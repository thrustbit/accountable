<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation;

use Thrustbit\Accountable\Application\Exceptions\InvalidOperation;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Event\ActivationTokenHasBeenExpired;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Event\NotActivatedUserWasRegistered;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Event\UserActivationWasActivated;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Event\UserActivationWasRenewed;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Exceptions\ActivationTokenExpired;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Exceptions\UserAlreadyActivated;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Values\ActivationToken;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class Activation extends ActivationModel
{
    public static function registerUser(UserId $userId, ActivationToken $token): self
    {
        $self = new self();
        $self->recordThat(NotActivatedUserWasRegistered::occur($userId->identify(), [
            'activation_token' => $token->identify()
        ]));

        return $self;
    }

    public function activate(): void
    {
        $status = $this->getActivationStatus();

        if ($status->isActivated()) {
            throw UserAlreadyActivated::withUserId($this->getId());
        }

        if ($status->isExpired()) {
            $this->recordThat(ActivationTokenHasBeenExpired::occur($this->getId()->identify(), [
                'activation_token' => $this->getActivationToken()->identify()
            ]));

            throw ActivationTokenExpired::withExpiredToken($this->getActivationToken());
        }

        $this->recordThat(UserActivationWasActivated::occur($this->getId()->identify(), [
            'activation_token' => null,
        ]));
    }

    public function renew(ActivationToken $token): void
    {
        if ($this->getActivationStatus()->isActivated()) {
            throw InvalidOperation::reason('User with id %s is activated', $this->getId()->identify());
        }

        if ($this->getActivationToken()->sameValueAs($token)) {
            throw InvalidOperation::reason('Activation token %s can not be used again.', $token->identify());
        }

        $this->recordThat(UserActivationWasRenewed::occur($this->getId()->identify(), [
            'activation_token' => $token->identify()
        ]));
    }

    protected function whenNotActivatedUserWasRegistered(NotActivatedUserWasRegistered $event): void
    {
        $this['id'] = $event->aggregateId();
        $this['token'] = $event->payload()['activation_token'];
        $this['created_at'] = $event->createdAt();
        $this['activated_at'] = null;
    }

    protected function whenUserActivationWasRenewed(UserActivationWasRenewed $event): void
    {
        $this['id'] = $event->aggregateId();
        $this['token'] = $event->payload()['activation_token'];
        $this['created_at'] = $event->createdAt();
        $this['activated_at'] = null;
    }

    protected function whenUserActivationWasActivated(UserActivationWasActivated $event): void
    {
        $this['token'] = $event->payload()['activation_token'];
        $this['activated_at'] = $event->createdAt();
    }

    protected function whenActivationTokenHasBeenExpired(ActivationTokenHasBeenExpired $event): void
    {
    }
}