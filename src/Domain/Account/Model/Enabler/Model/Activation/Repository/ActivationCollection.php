<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Repository;

use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Activation;

interface ActivationCollection
{
    public function get(string $aggregateId): ?Activation;

    public function save(Activation $root): void;
}