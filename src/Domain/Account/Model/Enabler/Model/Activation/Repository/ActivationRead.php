<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Repository;

use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Activation;
use Thrustbit\ModelEvent\Model\Repository\RepositoryRead;

interface ActivationRead extends RepositoryRead
{
    public function activationOfUserId(string $userId): ?Activation;

    public function activationOfToken(string $activationToken): ?Activation;
}