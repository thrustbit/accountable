<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Values;

use Thrustbit\DevDomain\Application\Values\Value;

class ActivationStatus implements Value
{
    /**
     * @var ActivationToken
     */
    private $token;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @var \DateTimeImmutable
     */
    private $activatedAt;

    public function __construct(\DateTimeImmutable $createdAt, ActivationToken $token = null, \DateTimeImmutable $activatedAt = null)
    {
        $this->token = $token;
        $this->createdAt = $createdAt;
        $this->activatedAt = $activatedAt;
    }

    public static function fromValues(\DateTimeImmutable $createdAt, ActivationToken $token = null, \DateTimeImmutable $activatedAt = null): self
    {
        return new self($createdAt, $token, $activatedAt);
    }

    public function isActivated(): bool
    {
        return null === $this->token && $this->activatedAt instanceof \DateTimeImmutable;
    }

    public function isNotActivated(): bool
    {
        return !$this->isActivated();
    }

    public function isExpired(): bool
    {
        return $this->isNotActivated()
            && $this->getCreatedAt()
                ->add(new \DateInterval(ActivationToken::TOKEN_EXPIRATION))
            < new \DateTimeImmutable();
    }

    public function isNotExpired(): bool
    {
        return !$this->isExpired();
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this
            && $this->token === $aValue->getToken()
            && $this->createdAt === $aValue->getCreatedAt()
            && $this->activatedAt === $aValue->getActivatedAt();
    }

    public function getToken(): ?ActivationToken
    {
        return $this->token;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getActivatedAt(): ?\DateTimeImmutable
    {
        return $this->activatedAt;
    }
}