<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Values;

use Thrustbit\DevDomain\Application\Exceptions\Validate;
use Thrustbit\DevDomain\Application\Values\Identifier;
use Thrustbit\DevDomain\Application\Values\Value;

class ActivationToken implements Identifier
{
    const TOKEN_LENGTH = 32;

    const TOKEN_EXPIRATION = 'PT12H';

    /**
     * @var string
     */
    private $token;

    private function __construct(string $token)
    {
        $this->token = $token;
    }

    public static function fromString($token): self
    {
        $message = 'Activation token is invalid';

        Validate::string($token, $message);
        Validate::length($token, self::TOKEN_LENGTH, $message);

        return new self($token);
    }

    public static function nextToken(): self
    {
        return new self(str_random(self::TOKEN_LENGTH));
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this && $this->token() === $aValue->token();
    }

    public function token(): string
    {
        return $this->token;
    }

    public function identify()
    {
        return $this->token();
    }

    public function __toString(): string
    {
        return $this->token();
    }
}