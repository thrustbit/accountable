<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Handler;

use React\Promise\Deferred;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Query\GetUserActivationByUserId;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Repository\ActivationCollection;

class GetUserActivationByUserIdHandler
{
    /**
     * @var ActivationCollection
     */
    private $activationCollection;

    public function __construct(ActivationCollection $activationCollection)
    {
        $this->activationCollection = $activationCollection;
    }

    public function __invoke(GetUserActivationByUserId $query, Deferred $deferred = null)
    {
        $activation = $this->activationCollection->get($query->userId()->identify());

        if (!$deferred) {
            return $activation;
        }

        $deferred->resolve($activation);
    }
}