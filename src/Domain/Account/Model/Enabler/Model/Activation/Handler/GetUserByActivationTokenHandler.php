<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Handler;

use React\Promise\Deferred;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Query\GetUserByActivationToken;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Repository\ActivationRead;

class GetUserByActivationTokenHandler
{
    /**
     * @var ActivationRead
     */
    private $activationRead;

    public function __construct(ActivationRead $activationRead)
    {
        $this->activationRead = $activationRead;
    }

    public function __invoke(GetUserByActivationToken $query, Deferred $deferred = null)
    {
        $activation = $this->activationRead->activationOfToken($query->activationToken()->identify());

        if (!$deferred) {
            return $activation;
        }

        $deferred->resolve($activation);
    }
}