<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Specification;

use Thrustbit\Accountable\Domain\Account\User;
use Thrustbit\DevDomain\Application\Specification\Composite;

class UserCanBeActivated extends Composite
{

    public function isSatisfiedBy($account): bool
    {
        if ($account instanceof User) {
            return $account->getUserStatus()->isActivated();
        }

        return false;
    }
}