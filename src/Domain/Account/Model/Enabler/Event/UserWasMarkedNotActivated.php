<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enabler\Event;

use Thrustbit\ModelEvent\ModelChanged;

class UserWasMarkedNotActivated extends ModelChanged
{
}