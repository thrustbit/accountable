<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Repository;

use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\PasswordReset;

interface ResetPasswordCollection
{
    public function get(string $aggregateId): ?PasswordReset;

    public function save(PasswordReset $model): void;
}