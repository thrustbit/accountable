<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Repository;

use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Filter\PasswordResetCollectionFilter;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\PasswordReset;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Values\PasswordResetToken;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;
use Thrustbit\ModelEvent\Model\Repository\RepositoryRead;

interface ResetPasswordRead extends RepositoryRead
{
    public function passwordResetOfId(string $aggregateId): ?PasswordReset;

    public function passwordResetOfUserEmail(EmailAddress $email): PasswordResetCollectionFilter;

    public function passwordResetOfToken(PasswordResetToken $token): ?PasswordReset;
}