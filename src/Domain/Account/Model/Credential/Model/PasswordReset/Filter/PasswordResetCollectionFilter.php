<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Filter;

use Illuminate\Support\Collection;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\PasswordReset;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Values\PasswordResetToken;

class PasswordResetCollectionFilter
{
    /**
     * @var Collection
     */
    private $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function lastPending(): ?PasswordReset
    {
        return $this->collection
            ->sortByDesc(function (PasswordReset $passwordReset) {
                return $passwordReset->createdAt();
            })
            ->first(function (PasswordReset $passwordReset) {
                return $passwordReset->getStatus()->isPending();
            });
    }

    public function matchFirstPending(PasswordResetToken $token): ?PasswordReset
    {
        return $this->collection->first(function (PasswordReset $passwordReset) use ($token) {
            $reset = $passwordReset->getStatus();

            return $reset->isPending() && $reset->getToken()->sameValueAs($token);
        });
    }

    public function allTrash(): Collection
    {
        return $this->collection->filter(function (PasswordReset $passwordReset) {
            return $passwordReset->getStatus()->isTrash();
        });
    }
}