<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Values\PasswordResetStatus;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Values\PasswordResetToken;
use Thrustbit\Accountable\Projection\AccountableStore;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;
use Thrustbit\ModelEvent\ModelRoot;

class PasswordResetTokenModel extends ModelRoot
{
    /**
     * @var string
     */
    protected $table = AccountableStore::USER_PASSWORD_RESET_TABLE;

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'email', 'token', 'created_at', 'reset_at'
    ];

    /**
     * @var array
     */
    protected $hidden = ['token'];

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var bool
     */
    public $incrementing = false;

    public function getId(): UuidInterface
    {
        return $this->getKey() instanceof UuidInterface
            ? $this->getKey()
            : Uuid::fromString($this->getKey());
    }

    public function getEmail(): EmailAddress
    {
        return $this['email'] instanceof EmailAddress
            ? $this['email']
            : EmailAddress::fromString($this['email']);
    }

    public function getPasswordResetToken(): ?PasswordResetToken
    {
        if (!$this['token']) {
            return null;
        }

        return $this['token'] instanceof PasswordResetToken
            ? $this['token']
            : PasswordResetToken::fromString($this['token']);
    }

    public function getResetAt(): ?\DateTimeImmutable
    {
        if (null === $this['reset_at']) {
            return null;
        }

        return $this['reset_at'] instanceof \DateTimeImmutable
            ? $this['reset_at']
            : new \DateTimeImmutable($this['reset_at']);
    }

    public function getStatus(): PasswordResetStatus
    {
        return PasswordResetStatus::fromValues(
            $this->createdAt(),
            $this->getPasswordResetToken(),
            $this->getResetAt()
        );
    }

    protected function aggregateId(): string
    {
        return $this->getId()->toString();
    }
}