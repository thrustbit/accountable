<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Exceptions;

use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Values\PasswordResetToken;

class InvalidPasswordResetToken extends PasswordResetException
{
    public static function withToken(PasswordResetToken $token): InvalidPasswordResetToken
    {
        return new self(
            sprintf('Password reset token is invalid or has been expired with token %s', $token->identify())
        );
    }
}