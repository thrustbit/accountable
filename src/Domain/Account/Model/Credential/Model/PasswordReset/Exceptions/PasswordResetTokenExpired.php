<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Exceptions;

use Ramsey\Uuid\UuidInterface;

class PasswordResetTokenExpired extends PasswordResetException
{
    public static function withId(UuidInterface $uuid): PasswordResetTokenExpired
    {
        return new self(
            sprintf('Password reset token has been expired with id %s', $uuid->toString())
        );
    }
}