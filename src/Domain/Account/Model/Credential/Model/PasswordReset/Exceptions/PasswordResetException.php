<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Exceptions;

use Thrustbit\Accountable\Domain\Account\Exceptions\UserException;

class PasswordResetException extends UserException
{
}