<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Exceptions;

use Thrustbit\Accountable\Application\Exceptions\InvalidArgumentException;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Values\PasswordResetToken;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;
use Thrustbit\DevDomain\Application\Values\Identifier;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class PasswordResetTokenNotFound extends PasswordResetException
{
    public static function withIdentifier(Identifier $identifier): PasswordResetTokenNotFound
    {
        switch ($identifier) {
            case $identifier instanceof UserId:
                $id = 'user id';
                break;
            case $identifier instanceof PasswordResetToken:
                $id = 'reset token';
                break;
            case $identifier instanceof EmailAddress:
                $id = 'email';
                break;
            default:
                throw InvalidArgumentException::unknownIdentifier($identifier);
        }

        return new self(
            sprintf('Password Reset not found with %s %s', $id, $identifier->identify())
        );
    }
}