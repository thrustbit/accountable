<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Command;

use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class DeactivateResetToken extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public function passwordResetId(): UuidInterface
    {
        return Uuid::fromString($this->payload()['password_reset_id']);
    }
}