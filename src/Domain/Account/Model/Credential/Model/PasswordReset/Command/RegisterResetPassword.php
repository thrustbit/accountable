<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Command;

use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;

class RegisterResetPassword extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public function email(): EmailAddress
    {
        return EmailAddress::fromString($this->payload()['email']);
    }
}