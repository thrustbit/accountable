<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Event;

use Thrustbit\ModelEvent\ModelChanged;

class PasswordResetWasDeactivated extends ModelChanged
{
}