<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Services;

use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\PasswordReset;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Values\PasswordResetToken;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;

interface FirstActiveResetTokenMatcher
{
    public function __invoke(EmailAddress $email, PasswordResetToken $token): ?PasswordReset;
}