<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Query;

use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\Common\Messaging\Query;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Values\PasswordResetToken;

class GetUserByPasswordResetToken extends Query implements PayloadConstructable
{
    use PayloadTrait;

    public function passwordResetToken(): PasswordResetToken
    {
        return PasswordResetToken::fromString($this->payload()['password_reset_token']);
    }
}