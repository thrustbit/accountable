<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset;

use Ramsey\Uuid\UuidInterface;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Event\PasswordResetWasDeactivated;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Event\PasswordResetWasRegistered;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Exceptions\PasswordResetTokenExpired;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Values\PasswordResetToken;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;

class PasswordReset extends PasswordResetTokenModel
{
    public static function registerResetPassword(UuidInterface $uuid, EmailAddress $email, PasswordResetToken $resetToken): self
    {
        $self = new self();
        $self->recordThat(PasswordResetWasRegistered::occur($uuid->toString(), [
            'email' => $email->identify(),
            'password_reset_token' => $resetToken->identify()
        ]));

        return $self;
    }

    public function resetPassword(): void
    {
        $this->recordThat(PasswordResetWasDeactivated::occur($this->aggregateId(), [
            'password_reset_token' => null,
        ]));

        if ($this->getStatus()->isTrash()) {
            throw PasswordResetTokenExpired::withId($this->getId());
        }
    }

    protected function whenPasswordResetWasRegistered(PasswordResetWasRegistered $event): void
    {
        $this['id'] = $event->aggregateId();
        $this['email'] = $event->payload()['email'];
        $this['token'] = $event->payload()['password_reset_token'];
        $this['created_at'] = $event->createdAt();
        $this['reset_at'] = null;
    }

    protected function whenPasswordResetWasDeactivated(PasswordResetWasDeactivated $event): void
    {
        $this['token'] = $event->payload()['password_reset_token'];
        $this['reset_at'] = $event->createdAt();
    }
}