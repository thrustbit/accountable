<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Handler;

use React\Promise\Deferred;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Query\GetPasswordResetByUserEmail;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Repository\ResetPasswordRead;

class GetPasswordResetByUserEmailHandler
{
    /**
     * @var ResetPasswordRead
     */
    private $resetPasswordRead;

    public function __construct(ResetPasswordRead $resetPasswordRead)
    {
        $this->resetPasswordRead = $resetPasswordRead;
    }

    public function __invoke(GetPasswordResetByUserEmail $query, Deferred $deferred = null)
    {
        $oneLastValid = $this->resetPasswordRead
            ->passwordResetOfUserEmail($query->email())
            ->lastPending();

        if (!$deferred) {
            return $oneLastValid;
        }

        $deferred->resolve($oneLastValid);
    }
}