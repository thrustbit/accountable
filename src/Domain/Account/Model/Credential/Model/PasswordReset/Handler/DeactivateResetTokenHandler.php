<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Handler;

use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Command\DeactivateResetToken;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Repository\ResetPasswordCollection;

class DeactivateResetTokenHandler
{
    /**
     * @var ResetPasswordCollection
     */
    private $resetPasswordCollection;

    public function __construct(ResetPasswordCollection $resetPasswordCollection)
    {
        $this->resetPasswordCollection = $resetPasswordCollection;
    }

    public function __invoke(DeactivateResetToken $command): void
    {
        // not in use by now
        $reset = $this->resetPasswordCollection->get($command->passwordResetId()->toString());

        $reset->deactivate();

        $this->resetPasswordCollection->save($reset);
    }
}