<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Handler;

use Thrustbit\Accountable\Domain\Account\Exceptions\UserNotFound;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Command\UserResetPassword;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Exceptions\PasswordResetTokenNotFound;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Repository\ResetPasswordCollection;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Services\FirstActiveResetTokenMatcher;
use Thrustbit\Accountable\Domain\Account\Repository\UserCollection;
use Thrustbit\Accountable\Domain\Account\Services\PasswordMaker;

class UserResetPasswordHandler
{
    /**
     * @var ResetPasswordCollection
     */
    private $resetPasswordCollection;

    /**
     * @var FirstActiveResetTokenMatcher
     */
    private $firstMatching;

    /**
     * @var UserCollection
     */
    private $userCollection;

    /**
     * @var PasswordMaker
     */
    private $passwordMaker;

    public function __construct(ResetPasswordCollection $resetPasswordCollection,
                                FirstActiveResetTokenMatcher $firstMatching,
                                UserCollection $userCollection,
                                PasswordMaker $passwordMaker)
    {
        $this->resetPasswordCollection = $resetPasswordCollection;
        $this->firstMatching = $firstMatching;
        $this->userCollection = $userCollection;
        $this->passwordMaker = $passwordMaker;
    }

    public function __invoke(UserResetPassword $command): void
    {
        $user = $this->userCollection->get($command->userId()->identify());

        if (!$user) {
            throw UserNotFound::withIdentifier($command->userId());
        }

        $reset = ($this->firstMatching)($user->getEmail(), $command->passwordResetToken());

        if (!$reset) {
            throw PasswordResetTokenNotFound::withIdentifier($user->getId());
        }

        $user->requestResetPassword($reset, $command->newPassword(), $this->passwordMaker);

        $this->resetPasswordCollection->save($reset);

        $this->userCollection->save($user);
    }
}