<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Handler;

use Ramsey\Uuid\Uuid;
use Thrustbit\Accountable\Domain\Account\Exceptions\UserNotFound;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Command\RegisterResetPassword;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Repository\ResetPasswordCollection;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Values\PasswordResetToken;
use Thrustbit\Accountable\Domain\Account\Repository\UserCollection;
use Thrustbit\Accountable\Domain\Account\Services\EmailAvailability;

class RegisterResetPasswordHandler
{
    /**
     * @var UserCollection
     */
    private $userCollection;

    /**
     * @var ResetPasswordCollection
     */
    private $passwordCollection;

    /**
     * @var EmailAvailability
     */
    private $uniqueEmail;

    public function __construct(UserCollection $userCollection, ResetPasswordCollection $passwordCollection, EmailAvailability $uniqueEmail)
    {
        $this->userCollection = $userCollection;
        $this->passwordCollection = $passwordCollection;
        $this->uniqueEmail = $uniqueEmail;
    }

    public function __invoke(RegisterResetPassword $command): void
    {
        $userId = ($this->uniqueEmail)($command->email());

        if (!$userId) {
            throw UserNotFound::withIdentifier($command->email());
        }

        $user = $this->userCollection->get($userId->identify());

        if(!$user){
            throw UserNotFound::withIdentifier($userId);
        }

        $reset = $user->registerResetPassword(Uuid::uuid4(), PasswordResetToken::nextToken());

        $this->passwordCollection->save($reset);
    }
}