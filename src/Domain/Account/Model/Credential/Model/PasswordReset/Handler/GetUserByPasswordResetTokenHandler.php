<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Handler;

use React\Promise\Deferred;
use Thrustbit\Accountable\Domain\Account\Exceptions\UserNotFound;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Exceptions\PasswordResetTokenNotFound;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Query\GetUserByPasswordResetToken;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Repository\ResetPasswordRead;
use Thrustbit\Accountable\Domain\Account\Repository\UserCollection;
use Thrustbit\Accountable\Domain\Account\Services\EmailAvailability;

class GetUserByPasswordResetTokenHandler
{
    /**
     * @var ResetPasswordRead
     */
    private $resetPasswordRead;

    /**
     * @var UserCollection
     */
    private $userCollection;

    /**
     * @var EmailAvailability
     */
    private $uniqueEmail;

    public function __construct(ResetPasswordRead $resetPasswordRead,
                                UserCollection $userCollection,
                                EmailAvailability $uniqueEmail)
    {
        $this->resetPasswordRead = $resetPasswordRead;
        $this->userCollection = $userCollection;
        $this->uniqueEmail = $uniqueEmail;
    }

    public function __invoke(GetUserByPasswordResetToken $query, Deferred $deferred = null)
    {
        $resetToken = $query->passwordResetToken();

        $reset = $this->resetPasswordRead->passwordResetOfToken($resetToken);

        if (!$reset) {
            throw PasswordResetTokenNotFound::withIdentifier($resetToken);
        }

        $email = $reset->getEmail();

        if (!$userId = ($this->uniqueEmail)($email)) {
            throw UserNotFound::withIdentifier($email);
        }

        $user = $this->userCollection->get($userId->identify());

        if (!$deferred) {
            return $user;
        }

        $deferred->resolve($user);
    }
}