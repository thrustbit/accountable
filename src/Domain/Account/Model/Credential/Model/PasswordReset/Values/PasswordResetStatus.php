<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Values;

use Thrustbit\DevDomain\Application\Values\Value;

class PasswordResetStatus implements Value
{
    /**
     * @var PasswordResetToken|null
     */
    private $token;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @var \DateTimeImmutable|null
     */
    private $resetAt;

    private function __construct(\DateTimeImmutable $createdAt,
                                 PasswordResetToken $token = null,
                                 \DateTimeImmutable $resetAt = null)
    {
        $this->createdAt = $createdAt;
        $this->token = $token;
        $this->resetAt = $resetAt;
    }

    public static function fromValues(\DateTimeImmutable $createdAt, PasswordResetToken $token = null, \DateTimeImmutable $resetAt = null)
    {
        return new self($createdAt, $token, $resetAt);
    }

    public function isPending(): bool
    {
        return $this->isActive() && $this->isNotExpired();
    }

    public function isTrash(): bool
    {
        return $this->isActive() && $this->isExpired();
    }

    public function isActive(): bool
    {
        return null !== $this->token && null === $this->resetAt;
    }

    public function isExpired(): bool
    {
        return $this->createdAt
                ->add(new \DateInterval(PasswordResetToken::TOKEN_EXPIRATION))
            < new \DateTimeImmutable();
    }

    public function isNotActive(): bool
    {
        return !$this->isActive();
    }

    public function isNotExpired(): bool
    {
        return !$this->isExpired();
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getToken(): ?PasswordResetToken
    {
        return $this->token;
    }

    public function getResetAt(): ?\DateTimeImmutable
    {
        return $this->resetAt;
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this
            && $this->token === $aValue->getToken()
            && $this->createdAt === $aValue->getCreatedAt()
            && $this->resetAt === $aValue->getResetAt();
    }
}