<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enrole\Services;

use Thrustbit\Accountable\Domain\Role\Role;
use Thrustbit\DevDomain\Application\Values\Identifier;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

interface UserRoleAvailable
{
    public function __invoke(UserId $userId, Identifier $identifier): ?Role;
}