<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enrole\Command;

use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Thrustbit\Accountable\Domain\Role\Values\RoleName;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class AttachRole extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public function userId(): UserId
    {
        return UserId::fromString($this->payload()['user_id']);
    }

    public function roleName(): RoleName
    {
        return RoleName::fromString($this->payload()['role_name']);
    }
}