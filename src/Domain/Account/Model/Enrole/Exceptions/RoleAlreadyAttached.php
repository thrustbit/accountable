<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enrole\Exceptions;

use Thrustbit\Accountable\Domain\Role\Exceptions\RoleException;
use Thrustbit\DevDomain\Application\Values\Identifier;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class RoleAlreadyAttached extends RoleException
{
    public static function withUserId(UserId $userId, Identifier $identifier): RoleAlreadyAttached
    {
        return new self(
            sprintf('Role %s already attached to user id %s', $identifier->identify(), $userId->identify())
        );
    }
}