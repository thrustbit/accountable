<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enrole;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Event\RoleWasAttached;
use Thrustbit\Accountable\Domain\Role\Values\RoleId;
use Thrustbit\Accountable\Projection\AccountableStore;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;
use Thrustbit\ModelEvent\ModelRoot;

class Enrole extends ModelRoot
{
    /**
     * @var string
     */
    protected $table = AccountableStore::ROLE_USER_TABLE;

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'user_id', 'role_id'
    ];


    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var bool
     */
    public $incrementing = false;

    public static function attachRole(UserId $userId, RoleId $roleId): self
    {
        $self = new self();
        $self->recordThat(RoleWasAttached::occur(Uuid::uuid4()->toString(), [
            'role_id' => $roleId->identify(),
            'user_id' => $userId->identify()
        ]));

        return $self;
    }

    public function getId(): UuidInterface
    {
        return $this->getKey() instanceof UuidInterface
            ? $this->getKey()
            : Uuid::fromString($this->getKey());
    }

    public function getUserId(): UserId
    {
        return $this['user_id'] instanceof UserId
            ? $this['user_id']
            : UserId::fromString($this['user_id']);
    }

    public function getRoleId(): RoleId
    {
        return $this['role_id'] instanceof RoleId
            ? $this['role_id']
            : RoleId::fromString($this['role_id']);
    }

    protected function aggregateId(): string
    {
        return $this->getId()->toString();
    }

    protected function whenRoleWasAttached(RoleWasAttached $event): void
    {
        $this['id'] = $event->aggregateId();
        $this['user_id'] = $event->payload()['user_id'];
        $this['role_id'] = $event->payload()['role_id'];
    }
}