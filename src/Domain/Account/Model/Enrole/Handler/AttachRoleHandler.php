<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enrole\Handler;

use Thrustbit\Accountable\Domain\Account\Exceptions\UserNotFound;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Command\AttachRole;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Repository\EnroleCollection;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Services\UserRoleAvailable;
use Thrustbit\Accountable\Domain\Account\Repository\UserCollection;
use Thrustbit\Accountable\Domain\Role\Role;
use Thrustbit\Accountable\Domain\Role\Values\RoleName;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class AttachRoleHandler
{
    /**
     * @var UserCollection
     */
    private $userCollection;

    /**
     * @var EnroleCollection
     */
    private $enroleCollection;

    /**
     * @var UserRoleAvailable
     */
    private $userHasRole;

    public function __construct(UserCollection $userCollection,
                                EnroleCollection $enroleCollection,
                                UserRoleAvailable $uniqueRoleAttacheToUser)
    {
        $this->enroleCollection = $enroleCollection;
        $this->userHasRole = $uniqueRoleAttacheToUser;
        $this->userCollection = $userCollection;
    }

    public function __invoke(AttachRole $command): void
    {
        $userId = $command->userId();

        if (!$user = $this->userCollection->get($userId->identify())) {
            throw UserNotFound::withIdentifier($userId);
        }

        $role = $this->requireRoleAvailability($userId, $command->roleName());

        $enrole = $user->giveRole($role);

        $this->enroleCollection->save($enrole);
    }

    private function requireRoleAvailability(UserId $userId, RoleName $roleName): ?Role
    {
        return ($this->userHasRole)($userId, $roleName);
    }
}