<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enrole\Event;

use Thrustbit\ModelEvent\ModelChanged;

class RoleWasAttached extends ModelChanged
{
}