<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enrole\Repository;

use Illuminate\Support\Collection;
use Thrustbit\Accountable\Application\Exceptions\InvalidArgumentException;
use Thrustbit\Accountable\Domain\Role\Role;
use Thrustbit\Accountable\Domain\Role\Values\RoleId;
use Thrustbit\Accountable\Domain\Role\Values\RoleName;
use Thrustbit\DevDomain\Application\Values\Identifier;

class EnroleCollectionFilter
{
    /**
     * @var Collection
     */
    private $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function hasRole(Identifier $identifier): bool
    {
        if ($identifier instanceof RoleName) {
            return $this->hasRoleName($identifier);
        }

        if ($identifier instanceof RoleId) {
            return $this->hasRoleId($identifier);
        }

        throw InvalidArgumentException::unknownIdentifier($identifier);
    }

    public function hasNotRole(Identifier $identifier): bool
    {
        return !$this->hasRole($identifier);
    }

    protected function hasRoleName(RoleName $roleName): bool
    {
        return $this->collection->filter(function (Role $role) use ($roleName) {
            return $role->getName()->sameValueAs($roleName);
        })->isNotEmpty();
    }

    protected function hasRoleId(RoleId $roleId): bool
    {
        return $this->collection->filter(function (Role $role) use ($roleId) {
            return $role->getId()->sameValueAs($roleId);
        })->isNotEmpty();
    }

    public function contains(Role $role): bool
    {
        return $this->collection->contains($role);
    }

    public function notContains(Role $role): bool
    {
        return !$this->collection->contains($role);
    }
}