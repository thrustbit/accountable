<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enrole\Repository;

use Thrustbit\Accountable\Domain\Account\Model\Enrole\Enrole;

interface EnroleCollection
{
    public function get(string $aggregateId): ?Enrole;

    public function save(Enrole $root): void;
}