<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Model\Enrole\Repository;

use Thrustbit\Accountable\Domain\Account\Model\Enrole\Enrole;
use Thrustbit\ModelEvent\Model\Repository\RepositoryRead;

interface EnroleRead extends RepositoryRead
{
    public function rolesOfUserId(string $userId): EnroleCollectionFilter;

    public function enroleOfId(string $aggregateId): ?Enrole;
}