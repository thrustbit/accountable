<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Event;

use Thrustbit\ModelEvent\ModelChanged;

class UserWasActivated extends ModelChanged
{
}