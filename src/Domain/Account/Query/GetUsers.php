<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Query;

use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\Common\Messaging\Query;

class GetUsers extends Query implements PayloadConstructable
{
    use PayloadTrait;
}