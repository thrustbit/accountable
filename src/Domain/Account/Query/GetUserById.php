<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Query;

use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\Common\Messaging\Query;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class GetUserById extends Query implements PayloadConstructable
{
    use PayloadTrait;

    public function userId(): UserId
    {
        return UserId::fromString($this->payload()['user_id']);
    }
}