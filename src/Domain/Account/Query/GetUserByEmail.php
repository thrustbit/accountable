<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Query;

use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\Common\Messaging\Query;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;

class GetUserByEmail extends Query implements PayloadConstructable
{
    use PayloadTrait;

    public function email(): EmailAddress
    {
        return $this->payload()['email'];
    }
}