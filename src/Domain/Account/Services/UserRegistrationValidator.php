<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Services;

use Thrustbit\Accountable\Domain\Account\Command\RegisterUser;
use Thrustbit\Accountable\Domain\Account\Exceptions\EmailAlreadyExists;
use Thrustbit\Accountable\Domain\Account\Exceptions\UserNameAlreadyExists;

class UserRegistrationValidator
{
    /**
     * @var EmailAvailability
     */
    private $uniqueEmail;

    /**
     * @var UserNameAvailability
     */
    private $uniqueUserName;

    public function __construct(EmailAvailability $uniqueEmail, UserNameAvailability $uniqueUserName)
    {
        $this->uniqueEmail = $uniqueEmail;
        $this->uniqueUserName = $uniqueUserName;
    }

    protected function userNameMustBeUnique(RegisterUser $command): void
    {
        if (null !== ($this->uniqueUserName)($command->userName())) {
            throw UserNameAlreadyExists::withUserName($command->userName());
        }
    }

    protected function emailAddressMustBeUnique(RegisterUser $command): void
    {
        if (null !== ($this->uniqueEmail)($command->email())) {
            throw EmailAlreadyExists::withEmail($command->email());
        }
    }

    public function __invoke(RegisterUser $command): void
    {
        $this->userNameMustBeUnique($command);

        $this->emailAddressMustBeUnique($command);
    }
}