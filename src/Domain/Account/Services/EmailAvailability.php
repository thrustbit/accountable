<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Services;

use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

interface EmailAvailability
{
    public function __invoke(EmailAddress $email): ?UserId;
}