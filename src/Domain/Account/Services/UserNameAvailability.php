<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Services;

use Thrustbit\DevDomain\Application\Values\Identity\UserId;
use Thrustbit\DevDomain\Application\Values\UserName\UserName;

interface UserNameAvailability
{
    public function __invoke(UserName $userName): ?UserId;
}