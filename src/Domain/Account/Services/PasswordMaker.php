<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Services;

use Thrustbit\DevDomain\Application\Values\Contracts\EncodedPassword;
use Thrustbit\DevDomain\Application\Values\Credentials\ClearPassword;

interface PasswordMaker
{
    public function check(ClearPassword $clearPassword, EncodedPassword $encodedPassword): bool;

    public function make(ClearPassword $clearPassword): EncodedPassword;

    public function reHash(ClearPassword $clearPassword, EncodedPassword $encodedPassword): ?EncodedPassword;

    public function __invoke(ClearPassword $clearPassword): EncodedPassword;
}