<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account;

use Ramsey\Uuid\UuidInterface;
use Thrustbit\Accountable\Application\Exceptions\InvalidOperation;
use Thrustbit\Accountable\Domain\Account\Event\EmailWasChanged;
use Thrustbit\Accountable\Domain\Account\Event\PasswordWasChanged;
use Thrustbit\Accountable\Domain\Account\Event\PasswordWasRehashed;
use Thrustbit\Accountable\Domain\Account\Event\PasswordWasReset;
use Thrustbit\Accountable\Domain\Account\Event\UserNameWasChanged;
use Thrustbit\Accountable\Domain\Account\Event\UserWasActivated;
use Thrustbit\Accountable\Domain\Account\Event\UserWasRegistered;
use Thrustbit\Accountable\Domain\Account\Exceptions\BadCredentials;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\PasswordReset;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Values\PasswordResetToken;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Event\UserWasMarkedNotActivated;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Exceptions\UserAlreadyActivated;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Enrole;
use Thrustbit\Accountable\Domain\Account\Values\UserStatus;
use Thrustbit\Accountable\Domain\Role\Role;
use Thrustbit\Accountable\Infrastructure\Services\BcryptPasswordMaker;
use Thrustbit\DevDomain\Application\Values\Credentials\BcryptPassword;
use Thrustbit\DevDomain\Application\Values\Credentials\ClearPassword;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;
use Thrustbit\DevDomain\Application\Values\Entity;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;
use Thrustbit\DevDomain\Application\Values\UserName\UserName;
use Thrustbit\ModelEvent\ModelChanged;

class User extends UserModel
{
    public static function registerUser(UserId $userId, EmailAddress $emailAddress, UserName $userName, BcryptPassword $password): self
    {
        $self = new self();
        $self->recordThat(UserWasRegistered::occur($userId->identify(), [
            'email' => $emailAddress->identify(),
            'user_name' => $userName->identify(),
            'password' => $password->getCredentials(),
            'user_status' => UserStatus::PENDING_REGISTRATION
        ]));

        return $self;
    }

    public function giveRole(Role $role): Enrole
    {
        return Enrole::attachRole($this->getId(), $role->getId());
    }

    public function markAsNotActivated(): void
    {
        if ($this->getUserStatus()->isNotPending()) {
            throw InvalidOperation::reason('User status must be in a pending registration state');
        }

        $this->recordThat(UserWasMarkedNotActivated::occur($this->aggregateId(), [
            'user_status' => UserStatus::NOT_ACTIVATED
        ]));
    }

    public function activateUser(): void
    {
        if ($this->getUserStatus()->isActivated()) {
            throw UserAlreadyActivated::withUserId($this->getId());
        }

        if ($this->getUserStatus()->isNotActivated()) {
            throw InvalidOperation::reason('User status must be in a not activated state');
        }

        $this->recordThat(UserWasActivated::occur($this->aggregateId(), [
            'user_status' => UserStatus::ACTIVATED
        ]));
    }

    public function changeEmail(EmailAddress $newEmail): void
    {
        $this->requireEnabledUser();

        if (!$this->getEmail()->sameValueAs($newEmail)) {
            $this->recordThat(EmailWasChanged::occur($this->aggregateId(), [
                'current_email' => $this->getEmail()->identify(),
                'new_email' => $newEmail->identify()
            ]));
        }
    }

    public function changeUserName(UserName $newUserName): void
    {
        $this->requireEnabledUser();

        if (!$this->getUserName()->sameValueAs($newUserName)) {
            $this->recordThat(UserNameWasChanged::occur($this->aggregateId(), [
                'old_user_name' => $this->getUserName()->identify(),
                'new_user_name' => $newUserName->identify()
            ]));
        }
    }

    public function changePassword(ClearPassword $currentPassword, ClearPassword $newPassword, BcryptPasswordMaker $passwordMaker): void
    {
        $this->requireEnabledUser();

        if (!$passwordMaker->check($currentPassword, $this->getPassword())) {
            throw BadCredentials::withUserId($this->getId());
        }

        if (!$newPassword->sameValueAs($this->getPassword())) {

            $newPassword = $passwordMaker->make($newPassword);

            $this->recordThat(PasswordWasChanged::occur($this->aggregateId(), [
                'old_password' => $this->getPassword()->getCredentials(),
                'new_password' => $newPassword->getCredentials()
            ]));
        } elseif (null !== $rehashedPassword = $passwordMaker->reHash($currentPassword, $this->getPassword())) {
            $this->recordThat(PasswordWasRehashed::occur($this->aggregateId(), [
                'old_password' => $this->getPassword()->getCredentials(),
                'new_password' => $rehashedPassword->getCredentials()
            ]));
        }
    }

    public function registerResetPassword(UuidInterface $uuid, PasswordResetToken $resetToken): PasswordReset
    {
        $this->requireEnabledUser();

        return PasswordReset::registerResetPassword($uuid, $this->getEmail(), $resetToken);
    }

    public function requestResetPassword(PasswordReset $passwordReset, ClearPassword $clearPassword, BcryptPasswordMaker $passwordMaker): void
    {
        $this->requireEnabledUser();

        $newPasswordEncoded = $passwordMaker->make($clearPassword);

        $passwordReset->resetPassword();

        $this->recordThat(PasswordWasReset::occur($this->aggregateId(), [
            'old_password' => $this->getPassword()->getCredentials(),
            'new_password' => $newPasswordEncoded->getCredentials()
        ]));
    }

    public function sameIdentityAs(Entity $aEntity): bool
    {
        return $aEntity instanceof $this
            && $this->getId()->sameValueAs($aEntity->getId());
    }

    protected function whenUserWasRegistered(UserWasRegistered $event): void
    {
        $this['id'] = $event->aggregateId();
        $this['user_name'] = $event->payload()['user_name'];
        $this['email'] = $event->payload()['email'];
        $this['password'] = $event->payload()['password'];
        $this['status'] = $event->payload()['user_status'];
        $this['created_at'] = $event->createdAt();
    }

    protected function whenUserWasMarkedNotActivated(UserWasMarkedNotActivated $event): void
    {
        $this->updateUserStatus($event);
    }

    protected function whenUserWasActivated(UserWasActivated $event): void
    {
        $this->updateUserStatus($event);
    }

    protected function whenEmailWasChanged(EmailWasChanged $event): void
    {
        $this['email'] = $event->payload()['new_email'];
        $this['updated_at'] = $event->createdAt();
    }

    protected function whenUserNameWasChanged(UserNameWasChanged $event): void
    {
        $this['user_name'] = $event->payload()['new_user_name'];
        $this['updated_at'] = $event->createdAt();
    }

    protected function whenPasswordWasChanged(PasswordWasChanged $event): void
    {
        $this->updatePassword($event);
    }

    protected function whenPasswordWasRehashed(PasswordWasRehashed $event): void
    {
        $this->updatePassword($event);
    }

    protected function whenPasswordWasReset(PasswordWasReset $event): void
    {
        $this->updatePassword($event);
    }

    private function updateUserStatus(ModelChanged $event): void
    {
        $this['status'] = $event->payload()['user_status'];
        $this['updated_at'] = $event->createdAt();
    }

    private function updatePassword(ModelChanged $event): void
    {
        $this['password'] = $event->payload()['new_password'];
        $this['updated_at'] = $event->createdAt();
    }
}