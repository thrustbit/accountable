<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Process;

use Prooph\ServiceBus\CommandBus;
use Thrustbit\Accountable\Domain\Account\Event\UserWasRegistered;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Command\CreateUserActivation;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Command\MarkUserAsNotActivated;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Values\ActivationToken;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Command\AttachRole;

class UserRegistrationProcess
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function onEvent(UserWasRegistered $event): void
    {
        $this->commandBus->dispatch(
            new CreateUserActivation([
                'user_id' => $event->aggregateId(),
                'activation_token' => ActivationToken::nextToken()->identify()
            ])
        );

        $this->commandBus->dispatch(
            new AttachRole([
                'user_id' => $event->aggregateId(),
                'role_name' => 'ROLE_USER' // FIX ME
            ])
        );

        $this->commandBus->dispatch(new MarkUserAsNotActivated([
            'user_id' => $event->aggregateId()
        ]));
    }
}