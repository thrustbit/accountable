<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Command;

use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Thrustbit\DevDomain\Application\Values\Credentials\ClearPassword;
use Thrustbit\DevDomain\Application\Values\Credentials\Password;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;
use Thrustbit\DevDomain\Application\Values\UserName\UserName;

class RegisterUser extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public function userId(): UserId
    {
        return UserId::fromString($this->payload['user_id']);
    }

    public function userName(): UserName
    {
        return UserName::fromString($this->payload['user_name']);
    }

    public function email(): EmailAddress
    {
        return EmailAddress::fromString($this->payload['email']);
    }

    public function password(): Password
    {
        return ClearPassword::fromStringWithConfirmation(
            $this->payload['password'],
            $this->payload['password_confirmation']
        );
    }
}