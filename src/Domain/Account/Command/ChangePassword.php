<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Command;

use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Thrustbit\DevDomain\Application\Values\Credentials\ClearPassword;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class ChangePassword extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public function userId(): UserId
    {
        return UserId::fromString($this->payload()['user_id']);
    }

    public function currentPassword(): ClearPassword
    {
        return ClearPassword::fromString($this->payload()['current_password']);
    }

    public function newPassword(): ClearPassword
    {
        return ClearPassword::fromStringWithConfirmation(
            $this->payload()['new_password'],
            $this->payload()['new_password_confirmation']
        );
    }
}