<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Command;

use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;
use Thrustbit\DevDomain\Application\Values\UserName\UserName;

class ChangeUserName extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public function userId(): UserId
    {
        return UserId::fromString($this->payload()['user_id']);
    }

    public function newUserName(): UserName
    {
        return UserName::fromString($this->payload()['new_user_name']);
    }
}