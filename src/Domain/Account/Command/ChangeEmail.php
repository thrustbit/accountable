<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Command;

use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;

class ChangeEmail extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public function userId(): UserId
    {
        return UserId::fromString($this->payload()['user_id']);
    }

    public function newEmail(): EmailAddress
    {
        return EmailAddress::fromString($this->payload()['new_email']);
    }
}