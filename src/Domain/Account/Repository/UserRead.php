<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Repository;

use Thrustbit\Accountable\Domain\Account\User;
use Thrustbit\DevDomain\Application\Values\Identifier;
use Thrustbit\ModelEvent\Model\Repository\RepositoryRead;

interface UserRead extends RepositoryRead
{
    public function userOfId(string $userId): ?User;

    public function userOfUserName(string $userName): ?User;

    public function userOfEmail(string $email): ?User;

    public function userOfIdentifier(Identifier $identifier): ?User;
}