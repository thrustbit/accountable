<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account\Repository;

use Thrustbit\Accountable\Domain\Account\User;

interface UserCollection
{
    public function get(string $userId): ?User;

    public function save(User $modelRoot): void;
}