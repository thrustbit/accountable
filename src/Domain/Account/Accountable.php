<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Domain\Account;

use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;
use Thrustbit\DevDomain\Application\Values\Identifier;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;
use Thrustbit\DevDomain\Application\Values\UserName\UserName;

interface Accountable
{
    public function getId(): UserId;

    public function getIdentifier(): Identifier;

    public function getUserName(): UserName;

    public function getEmail(): EmailAddress;

    public function getRoles(): array;
}