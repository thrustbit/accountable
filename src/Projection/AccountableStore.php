<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Projection;

use Illuminate\Database\Connection;
use Illuminate\Database\Query\Builder;
use Thrustbit\ModelEvent\ModelChanged;

abstract class AccountableStore
{
    const USER_TABLE = 'users';

    const USER_ACTIVATION_TABLE = 'user_activation';

    const USER_THROTTLE_TABLE = 'user_throttle';

    const USER_PASSWORD_RESET_TABLE = 'password_reset';

    const ROLE_TABLE = 'roles';

    const ROLE_USER_TABLE = 'role_user';

    /**
     * @var Connection
     */
    protected $connection;

    /**
     * @var int
     */
    protected $version = 0;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function onEvent(ModelChanged $event): void
    {
        $this->callOnEvent($event);
    }

    protected function create(array $data): void
    {
        if (!isset($data['version'])) {
            $data['version'] = $this->version;
        }

        $this->connect()->insert($data);
    }

    protected function update(string $aggregateId, array $data): void
    {
        if (!isset($data['version'])) {
            if (1 === $this->version) {
                throw new \RuntimeException('Event version to 1 probably wrong for updating event');
            }

            $data['version'] = $this->version;
        }

        $this->connect()->where('id', $aggregateId)->update($data);
    }

    protected function delete(string $aggregateId, array $data): void
    {
        $this->connect()->where('id', $aggregateId)->delete($data);
    }

    protected function formatDate(\DateTimeImmutable $dateTimeImmutable): string
    {
        return $dateTimeImmutable->format('Y-m-d H:i:s');
    }

    private function callOnEvent(ModelChanged $event): void
    {
        if (method_exists($this, $methodName = 'on' . class_basename($event))) {
            $this->setVersion($event->version());

            $this->{$methodName}($event);

            $this->resetVersion();
        } else {
            $this->raiseMissingEventHandlerException($event);
        }
    }

    protected function raiseMissingEventHandlerException(ModelChanged $event): \RuntimeException
    {
        throw new \RuntimeException(
            sprintf('Missing event handler %s for class %s', get_class($event), static::class)
        );
    }

    private function setVersion(int $eventVersion): void
    {
        $this->version = $eventVersion;
    }

    private function resetVersion(): void
    {
        $this->version = 0;
    }

    protected function version(): int
    {
        return $this->version;
    }

    abstract protected function connect(): Builder;
}