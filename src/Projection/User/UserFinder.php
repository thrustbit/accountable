<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Projection\User;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Thrustbit\Accountable\Application\Exceptions\InvalidArgumentException;
use Thrustbit\Accountable\Domain\Account\Repository\UserRead;
use Thrustbit\Accountable\Domain\Account\User;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;
use Thrustbit\DevDomain\Application\Values\Identifier;
use Thrustbit\DevDomain\Application\Values\Identity\UserId;
use Thrustbit\DevDomain\Application\Values\UserName\UserName;

class UserFinder implements UserRead
{
    /**
     * @var User|Model
     */
    private $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function userOfId(string $userId): ?User
    {
        return $this->newQuery()->find($userId);
    }

    public function userOfUserName(string $userName): ?User
    {
        return $this->newQuery()->where('user_name', $userName)->first();
    }

    public function userOfEmail(string $email): ?User
    {
        return $this->newQuery()->where('email', $email)->first();
    }

    public function userOfIdentifier(Identifier $identifier): ?User
    {
        if ($identifier instanceof UserId) {
            return $this->userOfId($identifier->identify());
        }

        if ($identifier instanceof EmailAddress) {
            return $this->userOfEmail($identifier->identify());
        }

        if ($identifier instanceof UserName) {
            return $this->userOfUserName($identifier->identify());
        }

        throw InvalidArgumentException::unknownIdentifier($identifier);
    }

    public function all(): Collection
    {
        return $this->newQuery()->get();
    }

    /**
     * @return Model|User
     */
    public function createModel(): Model
    {
        return $this->model->newInstance();
    }

    private function newQuery(): Builder
    {
        return $this->createModel()->newQuery();
    }
}