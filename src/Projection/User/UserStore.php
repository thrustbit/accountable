<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Projection\User;

use Illuminate\Database\Query\Builder;
use Thrustbit\Accountable\Domain\Account\Event\EmailWasChanged;
use Thrustbit\Accountable\Domain\Account\Event\PasswordWasChanged;
use Thrustbit\Accountable\Domain\Account\Event\PasswordWasRehashed;
use Thrustbit\Accountable\Domain\Account\Event\PasswordWasReset;
use Thrustbit\Accountable\Domain\Account\Event\UserNameWasChanged;
use Thrustbit\Accountable\Domain\Account\Event\UserWasActivated;
use Thrustbit\Accountable\Domain\Account\Event\UserWasRegistered;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Event\UserWasMarkedNotActivated;
use Thrustbit\Accountable\Projection\AccountableStore;
use Thrustbit\ModelEvent\ModelChanged;

class UserStore extends AccountableStore
{
    protected function OnUserWasRegistered(UserWasRegistered $event)
    {
        $this->create([
            'id' => $event->aggregateId(),
            'email' => $event->payload()['email'],
            'user_name' => $event->payload()['user_name'],
            'password' => $event->payload()['password'],
            'created_at' => $this->formatDate($event->createdAt())
        ]);
    }

    protected function onUserWasMarkedNotActivated(UserWasMarkedNotActivated $event): void
    {
        $this->update($event->aggregateId(), [
            'status' => $event->payload()['user_status'],
            'updated_at' => $this->formatDate($event->createdAt())
        ]);
    }

    protected function onUserWasActivated(UserWasActivated $event): void
    {
        $this->update($event->aggregateId(), [
            'status' => $event->payload()['user_status'],
            'updated_at' => $this->formatDate($event->createdAt())
        ]);
    }

    protected function onEmailWasChanged(EmailWasChanged $event): void
    {
        $this->update($event->aggregateId(), [
            'email' => $event->payload()['new_email'],
            'updated_at' => $this->formatDate($event->createdAt())
        ]);
    }

    protected function onUserNameWasChanged(UserNameWasChanged $event): void
    {
        $this->update($event->aggregateId(), [
            'user_name' => $event->payload()['new_user_name'],
            'updated_at' => $this->formatDate($event->createdAt())
        ]);
    }

    protected function onPasswordWasChanged(PasswordWasChanged $event): void
    {
        $this->updatePassword($event);
    }

    protected function onPasswordWasRehashed(PasswordWasRehashed $event): void
    {
        $this->updatePassword($event);
    }

    protected function onPasswordWasReset(PasswordWasReset $event): void
    {
        $this->updatePassword($event);
    }

    private function updatePassword(ModelChanged $event): void
    {
        $this->update($event->aggregateId(), [
            'password' => $event->payload()['new_password'],
            'updated_at' => $this->formatDate($event->createdAt())
        ]);
    }

    protected function connect(): Builder
    {
        return $this->connection->table(self::USER_TABLE);
    }
}