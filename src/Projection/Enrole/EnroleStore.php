<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Projection\Enrole;

use Illuminate\Database\Query\Builder;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Event\RoleWasAttached;
use Thrustbit\Accountable\Projection\AccountableStore;
use Thrustbit\ModelEvent\ModelChanged;

class EnroleStore extends AccountableStore
{
    protected function onRoleWasAttached(RoleWasAttached $event)
    {
        $this->create([
            'id' => $event->aggregateId(),
            'user_id' => $event->payload()['user_id'],
            'role_id' => $event->payload()['role_id']
        ]);
    }

    protected function connect(): Builder
    {
        return $this->connection->table(self::ROLE_USER_TABLE);
    }
}