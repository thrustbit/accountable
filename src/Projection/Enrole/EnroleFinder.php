<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Projection\Enrole;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Enrole;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Repository\EnroleCollectionFilter;
use Thrustbit\Accountable\Domain\Account\Model\Enrole\Repository\EnroleRead;
use Thrustbit\Accountable\Domain\Account\Repository\UserRead;

class EnroleFinder implements EnroleRead
{
    /**
     * @var Enrole
     */
    private $model;

    /**
     * @var UserRead
     */
    private $userRead;

    public function __construct(Enrole $model, UserRead $userRead)
    {
        $this->model = $model;
        $this->userRead = $userRead;
    }

    public function enroleOfId(string $aggregateId): ?Enrole
    {
        return $this->newQuery()->find($aggregateId); //need version pivot
    }

    public function rolesOfUserId(string $userId): EnroleCollectionFilter
    {
        $userRoles = $this->userRead->createModel()
            ->with('roles')
            ->find($userId);

        return new EnroleCollectionFilter($userRoles->roles);
    }

    /**
     * @return Model|Enrole
     */
    public function createModel(): Model
    {
        return $this->model->newInstance();
    }

    private function newQuery(): Builder
    {
        return $this->createModel()->newQuery();
    }
}