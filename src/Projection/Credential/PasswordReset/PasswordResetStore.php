<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Projection\Credential\PasswordReset;

use Illuminate\Database\Query\Builder;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Event\PasswordResetWasDeactivated;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Event\PasswordResetWasRegistered;
use Thrustbit\Accountable\Projection\AccountableStore;

class PasswordResetStore extends AccountableStore
{
    public function onPasswordResetWasRegistered(PasswordResetWasRegistered $event): void
    {
        $this->create([
            'id' => $event->aggregateId(),
            'email' => $event->payload()['email'],
            'token' => $event->payload()['password_reset_token'],
            'created_at' => $this->formatDate($event->createdAt())
        ]);
    }

    public function onPasswordResetWasDeactivated(PasswordResetWasDeactivated $event): void
    {
        $this->update($event->aggregateId(), [
            'token' => $event->payload()['password_reset_token'],
            'reset_at' => $this->formatDate($event->createdAt())
        ]);
    }

    protected function connect(): Builder
    {
        return $this->connection->table(self::USER_PASSWORD_RESET_TABLE);
    }
}