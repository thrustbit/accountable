<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Projection\Credential\PasswordReset;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Filter\PasswordResetCollectionFilter;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\PasswordReset;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Repository\ResetPasswordRead;
use Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Values\PasswordResetToken;
use Thrustbit\DevDomain\Application\Values\EmailAddress\EmailAddress;

class PasswordResetFinder implements ResetPasswordRead
{
    /**
     * @var PasswordReset
     */
    private $model;

    public function __construct(PasswordReset $model)
    {
        $this->model = $model;
    }

    public function passwordResetOfId(string $aggregateId): ?PasswordReset
    {
        return $this->newQuery()->find($aggregateId);
    }

    public function passwordResetOfUserEmail(EmailAddress $emailAddress): PasswordResetCollectionFilter
    {
        $resets = $this->newQuery()->where('email', $emailAddress->identify())->get();

        return new PasswordResetCollectionFilter($resets);
    }

    public function passwordResetOfToken(PasswordResetToken $token): ?PasswordReset
    {
        return $this->newQuery()->where('token', $token->identify())->first();
    }

    /**
     * @return Model|PasswordReset
     */
    public function createModel(): Model
    {
        return $this->model->newInstance();
    }

    private function newQuery(): Builder
    {
        return $this->createModel()->newQuery();
    }
}