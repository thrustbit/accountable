<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Projection\Enabler\Throttle;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Repository\ThrottleRead;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Throttle\Throttle;

class ThrottleFinder implements ThrottleRead
{
    /**
     * @var Throttle
     */
    private $model;

    public function __construct(Throttle $model)
    {
        $this->model = $model;
    }

    public function throttleOfUserId(string $userId): ?Throttle
    {
        return $this->newQuery()->find($userId);
    }

    /**
     * @return Model|Throttle
     */
    public function createModel(): Model
    {
        return $this->model->newInstance();
    }

    private function newQuery(): Builder
    {
        return $this->createModel()->newQuery();
    }
}