<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Projection\Enabler\Throttle;

use Illuminate\Database\Query\Builder;
use Thrustbit\Accountable\Projection\AccountableStore;
use Thrustbit\ModelEvent\ModelChanged;

class ThrottleStore extends AccountableStore
{

    public function onEvent(ModelChanged $event): void
    {
        $this->raiseMissingEventHandlerException($event);
    }

    protected function connect(): Builder
    {
        return $this->connection->table(self::USER_THROTTLE_TABLE);
    }
}