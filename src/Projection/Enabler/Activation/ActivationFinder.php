<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Projection\Enabler\Activation;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Activation;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Repository\ActivationRead;

class ActivationFinder implements ActivationRead
{
    /**
     * @var Activation
     */
    private $model;

    public function __construct(Activation $model)
    {
        $this->model = $model;
    }

    public function activationOfUserId(string $userId): ?Activation
    {
        return $this->newQuery()->find($userId);
    }

    public function activationOfToken(string $activationToken): ?Activation
    {
        return $this->newQuery()->where('token', $activationToken)->first();
    }

    /**
     * @return Model|Activation
     */
    public function createModel(): Model
    {
        return $this->model->newInstance();
    }

    private function newQuery(): Builder
    {
        return $this->createModel()->newQuery();
    }
}