<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Projection\Enabler\Activation;

use Illuminate\Database\Query\Builder;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Event\NotActivatedUserWasRegistered;
use Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Event\UserActivationWasActivated;
use Thrustbit\Accountable\Projection\AccountableStore;

class ActivationStore extends AccountableStore
{
    protected function onNotActivatedUserWasRegistered(NotActivatedUserWasRegistered $event): void
    {
        $this->create([
            'id' => $event->aggregateId(),
            'token' => $event->payload()['activation_token'],
            'created_at' => $this->formatDate($event->createdAt())
        ]);
    }

    protected function onUserActivationWasActivated(UserActivationWasActivated $event): void
    {
        $this->update($event->aggregateId(), [
            'token' => $event->payload()['activation_token'],
            'activated_at' => $this->formatDate($event->createdAt())
        ]);
    }

    protected function connect(): Builder
    {
        return $this->connection->table(self::USER_ACTIVATION_TABLE);
    }
}