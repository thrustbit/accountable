<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Projection\Role;

use Illuminate\Database\Query\Builder;
use Thrustbit\Accountable\Domain\Role\Event\RoleWasCreated;
use Thrustbit\Accountable\Projection\AccountableStore;
use Thrustbit\ModelEvent\ModelChanged;

class RoleStore extends AccountableStore
{
    protected function onRoleWasCreated(RoleWasCreated $event): void
    {
        $this->create([
            'id' => $event->aggregateId(),
            'name' => $event->payload()['role_name'],
            'slug' => $event->payload()['role_slug'],
            'description' => $event->payload()['role_description']
        ]);
    }

    protected function connect(): Builder
    {
        return $this->connection->table(self::ROLE_TABLE);
    }
}