<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Projection\Role;

use Illuminate\Contracts\Cache\Repository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Thrustbit\Accountable\Application\Exceptions\InvalidArgumentException;
use Thrustbit\Accountable\Domain\Role\Repository\RoleRead;
use Thrustbit\Accountable\Domain\Role\Role;
use Thrustbit\Accountable\Domain\Role\Values\RoleId;
use Thrustbit\Accountable\Domain\Role\Values\RoleName;
use Thrustbit\DevDomain\Application\Values\Identifier;

class RoleCacheFinder implements RoleRead
{
    /**
     * @var RoleFinder
     */
    private $roleFinder;

    /**
     * @var Repository
     */
    private $cache;

    public function __construct(RoleFinder $roleFinder, Repository $cache)
    {
        $this->roleFinder = $roleFinder;
        $this->cache = $cache;
    }

    public function createModel(): Model
    {
        return $this->roleFinder->createModel();
    }

    public function roleOfId(string $roleId): ?Role
    {
        return $this->fromCache()->first(function (Role $role) use ($roleId) {
            return $role->getId()->identify() === $roleId;
        });
    }

    public function roleOfName(string $roleName): ?Role
    {
        return $this->fromCache()->first(function (Role $role) use ($roleName) {
            return $role->getName()->identify() === $roleName;
        });
    }

    public function roleOfIdentifier(Identifier $identifier): ?Role
    {
        if ($identifier instanceof RoleName) {
            return $this->roleOfName($identifier->identify());
        }

        if ($identifier instanceof RoleId) {
            return $this->roleOfId($identifier->identify());
        }

        throw InvalidArgumentException::unknownIdentifier($identifier);
    }

    protected function fromCache(): Collection
    {
        if ($this->cache->has($this->cacheName())) {
            return $this->cache->get($this->cacheName());
        }

        $roles = $this->roleFinder->createModel()->newQuery()->get();

        $this->cache->forever($this->cacheName(), $roles);

        return $roles;
    }

    public function cacheName(): string
    {
        return sha1(get_class($this->createModel()));
    }
}