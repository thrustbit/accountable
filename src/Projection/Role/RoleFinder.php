<?php

declare(strict_types=1);

namespace Thrustbit\Accountable\Projection\Role;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Thrustbit\Accountable\Application\Exceptions\InvalidArgumentException;
use Thrustbit\Accountable\Domain\Role\Repository\RoleRead;
use Thrustbit\Accountable\Domain\Role\Role;
use Thrustbit\Accountable\Domain\Role\Values\RoleId;
use Thrustbit\Accountable\Domain\Role\Values\RoleName;
use Thrustbit\DevDomain\Application\Values\Identifier;

class RoleFinder implements RoleRead
{
    /**
     * @var Role
     */
    private $model;

    public function __construct(Role $model)
    {
        $this->model = $model;
    }

    public function roleOfId(string $roleId): ?Role
    {
        return $this->newQuery()->find($roleId);
    }

    public function roleOfName(string $roleName): ?Role
    {
        return $this->newQuery()->where('name', $roleName)->first();
    }

    public function roleOfIdentifier(Identifier $identifier): ?Role
    {
        if ($identifier instanceof RoleId) {
            return $this->roleOfId($identifier->identify());
        }

        if ($identifier instanceof RoleName) {
            return $this->roleOfName($identifier->identify());
        }

        throw InvalidArgumentException::unknownIdentifier($identifier);
    }

    /**
     * @return Model|Role
     */
    public function createModel(): Model
    {
        return $this->model->newInstance();
    }

    private function newQuery(): Builder
    {
        return $this->createModel()->newQuery();
    }
}