<?php

return [
    'messages' => [

        'command_api' => [
            'user-register' => \Thrustbit\Accountable\Domain\Account\Command\RegisterUser::class,
            'user-activate_by_token' => \Thrustbit\Accountable\Domain\Account\Model\Enabler\Command\ActivateUserWithActivationToken::class,
            'user-profile-change_password' => \Thrustbit\Accountable\Domain\Account\Command\ChangePassword::class,
            'user-profile-change_email' => \Thrustbit\Accountable\Domain\Account\Command\ChangeEmail::class,
            'user-reset_password' => \Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Command\RegisterResetPassword::class,
            'user-reset_password_sender' => \Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Command\UserResetPassword::class,

        ],

        'query_api' => [
            'users-all' => \Thrustbit\Accountable\Domain\Account\Query\GetUsers::class
        ]
    ]
];