<?php

return [

    'laraprooph' => [

        'defaults' => [
            'command' => 'command_bus',
            'query' => 'query_bus',
            'event' => 'event_bus',
        ],

        /**
         * Global options merge with bus options
         */
        'options' => [

            'message_factory' => \Prooph\Common\Messaging\MessageFactory::class,
            'plugins' => []
        ],

        'dispatcher' => [

            'command' => [

                'command_bus' => [

                    'concrete' => \Prooph\ServiceBus\CommandBus::class,
                    'router' => [
                        'concrete' => \Prooph\ServiceBus\Plugin\Router\CommandRouter::class
                    ],
                    'options' => [
                        'plugins' => [
                            \Thrustbit\ModelEvent\EventLog\Plugin\TransactionManager::class,
                        ]
                    ]
                ],
            ],

            'event' => [

                'event_bus' => [

                    'concrete' => \Prooph\ServiceBus\EventBus::class,
                    'router' => [
                        'concrete' => \Prooph\ServiceBus\Plugin\Router\EventRouter::class
                    ],
                    'options' => [
                        'plugins' => [
                            \Prooph\ServiceBus\Plugin\InvokeStrategy\OnEventStrategy::class
                        ]
                    ]
                ]
            ],

            'query' => [

                'query_bus' => [

                    'concrete' => \Prooph\ServiceBus\QueryBus::class,
                    'router' => [
                        'concrete' => \Prooph\ServiceBus\Plugin\Router\QueryRouter::class
                    ],
                    'options' => [
                        'plugins' => []
                    ]
                ],
            ]
        ],

        'router' => [

            /**
             * Routes for command bus service id
             */
            'command_bus' => [

                'router' => [
                    'routes' => [

                        // Console
                        \Thrustbit\Accountable\Domain\Role\Command\CreateRole::class => \Thrustbit\Accountable\Domain\Role\Handler\CreateRoleHandler::class,

                        // App

                        \Thrustbit\Accountable\Domain\Account\Command\RegisterUser::class => \Thrustbit\Accountable\Domain\Account\Handler\RegisterUserHandler::class,
                        \Thrustbit\Accountable\Domain\Account\Model\Enabler\Command\CreateUserActivation::class => \Thrustbit\Accountable\Domain\Account\Model\Enabler\Handler\CreateUserActivationHandler::class,
                        \Thrustbit\Accountable\Domain\Account\Model\Enabler\Command\MarkUserAsNotActivated::class => \Thrustbit\Accountable\Domain\Account\Model\Enabler\Handler\MarkUserAsNotActivatedHandler::class,

                        \Thrustbit\Accountable\Domain\Account\Model\Enrole\Command\AttachRole::class => \Thrustbit\Accountable\Domain\Account\Model\Enrole\Handler\AttachRoleHandler::class,

                        \Thrustbit\Accountable\Domain\Account\Model\Enabler\Command\ActivateUserWithActivationToken::class => \Thrustbit\Accountable\Domain\Account\Model\Enabler\Handler\ActivateUserWithActivationTokenHandler::class,
                        \Thrustbit\Accountable\Domain\Account\Command\ActivateUser::class => \Thrustbit\Accountable\Domain\Account\Handler\ActivateUserHandler::class,

                        \Thrustbit\Accountable\Domain\Account\Command\ChangeEmail::class => \Thrustbit\Accountable\Domain\Account\Handler\ChangeEmailHandler::class,
                        \Thrustbit\Accountable\Domain\Account\Command\ChangeUserName::class => \Thrustbit\Accountable\Domain\Account\Handler\ChangeUserNameHandler::class,
                        \Thrustbit\Accountable\Domain\Account\Command\ChangePassword::class => \Thrustbit\Accountable\Domain\Account\Handler\ChangePasswordHandler::class,

                        \Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Command\RegisterResetPassword::class => \Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Handler\RegisterResetPasswordHandler::class,
                        \Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Command\UserResetPassword::class => \Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Handler\UserResetPasswordHandler::class,
                    ]
                ]
            ],

            /**
             * Routes for query bus service id
             */
            'query_bus' => [

                'router' => [
                    'routes' => [
                        \Thrustbit\Accountable\Domain\Account\Query\GetUserById::class => \Thrustbit\Accountable\Domain\Account\Handler\GetUserByIdHandler::class,
                        \Thrustbit\Accountable\Domain\Account\Query\GetUserByEmail::class => \Thrustbit\Accountable\Domain\Account\Handler\GetUserByEmailHandler::class,
                        \Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Query\GetUserByActivationToken::class => \Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Handler\GetUserByActivationTokenHandler::class,
                        \Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Query\GetUserActivationByUserId::class => \Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Handler\GetUserActivationByUserIdHandler::class,
                        \Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Query\GetUserByPasswordResetToken::class => \Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Handler\GetUserByPasswordResetTokenHandler::class,
                        \Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Query\GetPasswordResetByUserEmail::class => \Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Handler\GetPasswordResetByUserEmailHandler::class,

                        \Thrustbit\Accountable\Domain\Account\Query\GetUsers::class => \Thrustbit\Accountable\Domain\Account\Handler\GetUsersHandler::class,
                    ]
                ]
            ],

            /**
             * Routes for event bus service id
             */
            'event_bus' => [

                'router' => [
                    'routes' => [

                        //

                        \Thrustbit\Accountable\Domain\Account\Event\UserWasRegistered::class => [
                            \Thrustbit\Accountable\Projection\User\UserStore::class,
                            \Thrustbit\Accountable\Domain\Account\Process\UserRegistrationProcess::class,
                        ],

                        \Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Event\NotActivatedUserWasRegistered::class => [
                            \Thrustbit\Accountable\Projection\Enabler\Activation\ActivationStore::class
                        ],

                        \Thrustbit\Accountable\Domain\Account\Model\Enabler\Model\Activation\Event\UserActivationWasActivated::class => [
                            \Thrustbit\Accountable\Projection\Enabler\Activation\ActivationStore::class
                        ],

                        \Thrustbit\Accountable\Domain\Account\Event\UserWasActivated::class => [
                            \Thrustbit\Accountable\Projection\User\UserStore::class
                        ],

                        \Thrustbit\Accountable\Domain\Account\Model\Enabler\Event\UserWasMarkedNotActivated::class => [
                            \Thrustbit\Accountable\Projection\User\UserStore::class
                        ],


                        //

                        \Thrustbit\Accountable\Domain\Role\Event\RoleWasCreated::class => [
                            \Thrustbit\Accountable\Projection\Role\RoleStore::class
                        ],

                        \Thrustbit\Accountable\Domain\Account\Model\Enrole\Event\RoleWasAttached::class => [
                            \Thrustbit\Accountable\Projection\Enrole\EnroleStore::class
                        ],

                        \Thrustbit\Accountable\Domain\Account\Event\PasswordWasChanged::class => [
                            \Thrustbit\Accountable\Projection\User\UserStore::class
                        ],

                        \Thrustbit\Accountable\Domain\Account\Event\PasswordWasRehashed::class => [
                            \Thrustbit\Accountable\Projection\User\UserStore::class
                        ],

                        \Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Event\PasswordResetWasRegistered::class => [
                            \Thrustbit\Accountable\Projection\Credential\PasswordReset\PasswordResetStore::class,
                            // need process to send token
                        ],

                        \Thrustbit\Accountable\Domain\Account\Event\PasswordWasReset::class => [
                            \Thrustbit\Accountable\Projection\User\UserStore::class
                        ],

                        \Thrustbit\Accountable\Domain\Account\Model\Credential\Model\PasswordReset\Event\PasswordResetWasDeactivated::class => [
                            \Thrustbit\Accountable\Projection\Credential\PasswordReset\PasswordResetStore::class
                        ],

                        \Thrustbit\Accountable\Domain\Account\Event\EmailWasChanged::class => [
                            \Thrustbit\Accountable\Projection\User\UserStore::class
                        ],

                        \Thrustbit\Accountable\Domain\Account\Event\UserNameWasChanged::class => [
                            \Thrustbit\Accountable\Projection\User\UserStore::class
                        ],
                    ]
                ]
            ],

        ]
    ]
];