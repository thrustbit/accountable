<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasswordResetTable extends Migration
{

    public function up(): void
    {
        Schema::create('password_reset', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('email');
            $table->string('token')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('reset_at')->nullable();

            $table->integer('version', false, true)->default(1);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('password_reset');
    }
}