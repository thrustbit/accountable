<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserActivationTable extends Migration
{

    public function up(): void
    {
        Schema::create('user_activation', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('token')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('activated_at')->nullable();

            $table->integer('version', false, true)->default(1);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_activation');
    }
}
