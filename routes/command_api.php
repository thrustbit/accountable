<?php

use Illuminate\Support\Facades\Route;

Route::post('user/register')->name('user-register');

Route::post('user/activate_user/{activation_token}')->name('user-activate_by_token');

Route::post('user/profile/change_password')->name('user-profile-change_password');

Route::post('user/profile/change_email')->name('user-profile-change_email');

Route::post('user/profile/reset_password_user/{password_reset_token}')->name('user-reset_password');

Route::post('user/reset_password_user')->name('user-reset_password_sender');