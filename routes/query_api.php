<?php

/*
Route::any('users/unique/{user_id}/user_id')->name('users-unique_user_id');
Route::any('users/unique/{user_email}/user_email')->name('users-unique_email');
Route::any('users/unique/{user_name}/user_name')->name('users-unique_user_name');
*/

Route::any('users/{user_id}')->name('users-user_by_id');
Route::any('users')->name('users-all');

